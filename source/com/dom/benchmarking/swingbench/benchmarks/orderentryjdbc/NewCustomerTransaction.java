package com.dom.benchmarking.swingbench.benchmarks.orderentryjdbc;


import com.dom.benchmarking.swingbench.event.JdbcTaskEvent;
import com.dom.benchmarking.swingbench.kernel.SwingBenchException;
import com.dom.benchmarking.swingbench.utilities.RandomGenerator;
import com.dom.util.Utilities;

import java.io.File;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;


public class NewCustomerTransaction extends OrderEntryProcess {
    private static final Logger logger = Logger.getLogger(NewCustomerTransaction.class.getName());
    private static final String COUNTIES_FILE = "data/counties.txt";
    private static final String COUNTRIES_FILE = "data/countries.txt";
    private static final String FIRST_NAMES_FILE = "data/lowerfirstnames.txt";
    private static final String LAST_NAMES_FILE = "data/lowerlastnames.txt";
    private static final String NLS_FILE = "data/nls.txt";
    private static final String TOWNS_FILE = "data/towns.txt";
    private static List<NLSSupport> nlsInfo = new ArrayList<>();
    private static List<String> counties = null;
    private static List<String> countries = null;
    private static List<String> firstNames = null;
    private static List<String> lastNames = null;
    private static List<String> nlsInfoRaw = null;
    private static List<String> towns = null;
    private static final Object lock = new Object();

    public void init(Map<String, Object> params) throws SwingBenchException {
        boolean initCompleted = false;

        if ((firstNames == null) || !initCompleted) { // load any data you might need (in this case only once)

            synchronized (lock) {
                if (firstNames == null) {

                    String value = (String) params.get("SOE_FIRST_NAMES_LOC");
                    File firstNamesFile = new File((value == null) ? FIRST_NAMES_FILE : value);
                    value = (String) params.get("SOE_LAST_NAMES_LOC");
                    File lastNamesFile = new File((value == null) ? LAST_NAMES_FILE : value);
                    value = (String) params.get("SOE_NLSDATA_LOC");
                    File nlsFile = new File((value == null) ? NLS_FILE : value);
                    value = (String) params.get("SOE_TOWNS_LOC");
                    File townsFile = new File((value == null) ? TOWNS_FILE : value);
                    value = (String) params.get("SOE_COUNTIES_LOC");
                    File countiesFile = new File((value == null) ? COUNTIES_FILE : value);
                    value = (String) params.get("SOE_COUNTRIES_LOC");
                    File countriesFile = new File((value == null) ? COUNTRIES_FILE : value);

                    try {
                        firstNames = Utilities.cacheFile(firstNamesFile);
                        lastNames = Utilities.cacheFile(lastNamesFile);
                        nlsInfoRaw = Utilities.cacheFile(nlsFile);
                        counties = Utilities.cacheFile(countiesFile);
                        towns = Utilities.cacheFile(townsFile);
                        countries = Utilities.cacheFile(countriesFile);

                        for (String rawData : nlsInfoRaw) {
                            NewCustomerTransaction.NLSSupport nls = new NLSSupport();
                            StringTokenizer st = new StringTokenizer(rawData, ",");
                            nls.language = st.nextToken();
                            nls.territory = st.nextToken();
                            nlsInfo.add(nls);
                        }
                        logger.fine(
                                "Completed reading files needed for initialisation of " +
                                "NewCustomerTransaction()");

                    } catch (java.io.FileNotFoundException fne) {
                        logger.log(Level.SEVERE, "Unable to open data seed files : ", fne);
                        throw new SwingBenchException(fne);
                    } catch (java.io.IOException ioe) {
                        logger.log(Level.SEVERE, "IO problem opening seed files : ", ioe);
                        throw new SwingBenchException(ioe);
                    }
                }

                initCompleted = true;
            }
        }
    }

    public void execute(Map<String, Object> params) throws SwingBenchException {
        Connection connection = (Connection) params.get(JDBC_CONNECTION);
        long custID;
        long addressID;
        String firstName = firstNames.get(RandomGenerator.randomInteger(0, firstNames.size()));
        String lastName = lastNames.get(RandomGenerator.randomInteger(0, lastNames.size()));
        NewCustomerTransaction.NLSSupport nls = nlsInfo.get(RandomGenerator.randomInteger(0, nlsInfo.size()));

        String sql = "insert into %s (customer_id,\n" +
                     "                       cust_first_name,\n" +
                     "                       cust_last_name,\n" +
                     "                       nls_language,\n" +
                     "                       nls_territory,\n" +
                     "                       credit_limit,\n" +
                     "                       cust_email,\n" +
                     "                       account_mgr_id,\n" +
                     "                       customer_since,\n" +
                     "                       customer_class,\n" +
                     "                       suggestions,\n" +
                     "                       dob,\n" + "                       mailshot,\n" +
                     "                       partner_mailshot,\n" +
                     "                       preferred_address,\n" +
                     "                       preferred_card)\n" +
                     "values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

        initJdbcTask();

        long executeStart = System.nanoTime();
        try {
            try (PreparedStatement seqPs = connection.prepareStatement("select customer_seq.nextval, address_seq.nextval from dual");
                 PreparedStatement insPs1 =
                         connection.prepareStatement(String.format(sql,"CUSTOMERS1")); PreparedStatement insPs2 = connection.prepareStatement(String.format(sql,"CUSTOMERS2"));
                     PreparedStatement insPs3 = connection.prepareStatement(String.format(sql,
                                                                                          "CUSTOMERS3")); PreparedStatement insPs4 = connection.prepareStatement(String.format(sql,"CUSTOMERS4"));
                     PreparedStatement insPs5 = connection.prepareStatement(String.format(sql,
                                                                                          "CUSTOMERS5"));
                 PreparedStatement insPs6 = connection.prepareStatement(String.format(sql,
                                                                                      "CUSTOMERS6"));
                 PreparedStatement insPs7 = connection.prepareStatement(String.format(sql,
                                                                                      "CUSTOMERS7"));
                 PreparedStatement insPs8 = connection.prepareStatement(String.format(sql,
                                                                                      "CUSTOMERS8"));
                 PreparedStatement insPs9 = connection.prepareStatement(String.format(sql,
                                                                                      "CUSTOMERS9"));
                 PreparedStatement insPs10 = connection.prepareStatement(String.format(sql,
                                                                                      "CUSTOMERS10"));
//                 PreparedStatement insPs11 = connection.prepareStatement(String.format(sql,
//                                                                                      "CUSTOMERS11"));
//                 PreparedStatement insPs12= connection.prepareStatement(String.format(sql,
//                                                                                      "CUSTOMERS12"));
//                 PreparedStatement insPs13= connection.prepareStatement(String.format(sql,
//                                                                                      "CUSTOMERS13"));
//                 PreparedStatement insPs14= connection.prepareStatement(String.format(sql,
//                                                                                      "CUSTOMERS14"));
//                 PreparedStatement insPs15 = connection.prepareStatement(String.format(sql,
//                                                                                      "CUSTOMERS15"));
                     ResultSet rs = seqPs.executeQuery()) {

                        rs.next();
                        custID = rs.getLong(1);
                        addressID = rs.getLong(2);

                        addSelectStatements(1);
                        thinkSleep();

                        Date dob = new Date(System.currentTimeMillis() -
                                            (RandomGenerator.randomLong(18, 65) * 31556952000L));
                        Date custSince = new Date(System.currentTimeMillis() -
                                                  (RandomGenerator.randomLong(1, 4) * 31556952000L));

                        insPs1.setLong(1, custID);
                        insPs1.setString(2, firstName);
                        insPs1.setString(3, lastName);
                        insPs1.setString(4, nls.language);
                        insPs1.setString(5, nls.territory);
                        insPs1.setInt(6,
                                      RandomGenerator.randomInteger(MIN_CREDITLIMIT, MAX_CREDITLIMIT));
                        insPs1.setString(7, firstName + "." + lastName + "@" + "oracle.com");
                        insPs1.setInt(8, RandomGenerator.randomInteger(MIN_SALESID, MAX_SALESID));
                        insPs1.setDate(9, custSince);
                        insPs1.setString(10, "Ocasional");
                        insPs1.setString(11, "Music");
                        insPs1.setDate(12, dob);
                        insPs1.setString(13, "Y");
                        insPs1.setString(14, "N");
                        insPs1.setLong(15, addressID);
                        insPs1.setLong(16, custID);

                        insPs1.execute();
                        addInsertStatements(1);

                        insPs2.setLong(1, custID);
                        insPs2.setString(2, firstName);
                        insPs2.setString(3, lastName);
                        insPs2.setString(4, nls.language);
                        insPs2.setString(5, nls.territory);
                        insPs2.setInt(6,
                                      RandomGenerator.randomInteger(MIN_CREDITLIMIT, MAX_CREDITLIMIT));
                        insPs2.setString(7, firstName + "." + lastName + "@" + "oracle.com");
                        insPs2.setInt(8, RandomGenerator.randomInteger(MIN_SALESID, MAX_SALESID));
                        insPs2.setDate(9, custSince);
                        insPs2.setString(10, "Ocasional");
                        insPs2.setString(11, "Music");
                        insPs2.setDate(12, dob);
                        insPs2.setString(13, "Y");
                        insPs2.setString(14, "N");
                        insPs2.setLong(15, addressID);
                        insPs2.setLong(16, custID);

                        insPs2.execute();
                        addInsertStatements(1);

                        insPs3.setLong(1, custID);
                        insPs3.setString(2, firstName);
                        insPs3.setString(3, lastName);
                        insPs3.setString(4, nls.language);
                        insPs3.setString(5, nls.territory);
                        insPs3.setInt(6,
                                      RandomGenerator.randomInteger(MIN_CREDITLIMIT, MAX_CREDITLIMIT));
                        insPs3.setString(7, firstName + "." + lastName + "@" + "oracle.com");
                        insPs3.setInt(8, RandomGenerator.randomInteger(MIN_SALESID, MAX_SALESID));
                        insPs3.setDate(9, custSince);
                        insPs3.setString(10, "Ocasional");
                        insPs3.setString(11, "Music");
                        insPs3.setDate(12, dob);
                        insPs3.setString(13, "Y");
                        insPs3.setString(14, "N");
                        insPs3.setLong(15, addressID);
                        insPs3.setLong(16, custID);

                        insPs3.execute();
                        addInsertStatements(1);

                        insPs4.setLong(1, custID);
                        insPs4.setString(2, firstName);
                        insPs4.setString(3, lastName);
                        insPs4.setString(4, nls.language);
                        insPs4.setString(5, nls.territory);
                        insPs4.setInt(6,
                                      RandomGenerator.randomInteger(MIN_CREDITLIMIT, MAX_CREDITLIMIT));
                        insPs4.setString(7, firstName + "." + lastName + "@" + "oracle.com");
                        insPs4.setInt(8, RandomGenerator.randomInteger(MIN_SALESID, MAX_SALESID));
                        insPs4.setDate(9, custSince);
                        insPs4.setString(10, "Ocasional");
                        insPs4.setString(11, "Music");
                        insPs4.setDate(12, dob);
                        insPs4.setString(13, "Y");
                        insPs4.setString(14, "N");
                        insPs4.setLong(15, addressID);
                        insPs4.setLong(16, custID);

                        insPs4.execute();
                        addInsertStatements(1);

                        insPs5.setLong(1, custID);
                        insPs5.setString(2, firstName);
                        insPs5.setString(3, lastName);
                        insPs5.setString(4, nls.language);
                        insPs5.setString(5, nls.territory);
                        insPs5.setInt(6,
                                      RandomGenerator.randomInteger(MIN_CREDITLIMIT, MAX_CREDITLIMIT));
                        insPs5.setString(7, firstName + "." + lastName + "@" + "oracle.com");
                        insPs5.setInt(8, RandomGenerator.randomInteger(MIN_SALESID, MAX_SALESID));
                        insPs5.setDate(9, custSince);
                        insPs5.setString(10, "Ocasional");
                        insPs5.setString(11, "Music");
                        insPs5.setDate(12, dob);
                        insPs5.setString(13, "Y");
                        insPs5.setString(14, "N");
                        insPs5.setLong(15, addressID);
                        insPs5.setLong(16, custID);

                        insPs5.execute();
                        addInsertStatements(1);


                        insPs6.setLong(1, custID);
                        insPs6.setString(2, firstName);
                        insPs6.setString(3, lastName);
                        insPs6.setString(4, nls.language);
                        insPs6.setString(5, nls.territory);
                        insPs6.setInt(6,
                              RandomGenerator.randomInteger(MIN_CREDITLIMIT, MAX_CREDITLIMIT));
                        insPs6.setString(7, firstName + "." + lastName + "@" + "oracle.com");
                        insPs6.setInt(8, RandomGenerator.randomInteger(MIN_SALESID, MAX_SALESID));
                        insPs6.setDate(9, custSince);
                        insPs6.setString(10, "Ocasional");
                        insPs6.setString(11, "Music");
                        insPs6.setDate(12, dob);
                        insPs6.setString(13, "Y");
                        insPs6.setString(14, "N");
                        insPs6.setLong(15, addressID);
                        insPs6.setLong(16, custID);

                        insPs6.execute();
                        addInsertStatements(1);


                        insPs7.setLong(1, custID);
                        insPs7.setString(2, firstName);
                        insPs7.setString(3, lastName);
                        insPs7.setString(4, nls.language);
                        insPs7.setString(5, nls.territory);
                        insPs7.setInt(6,
                              RandomGenerator.randomInteger(MIN_CREDITLIMIT, MAX_CREDITLIMIT));
                        insPs7.setString(7, firstName + "." + lastName + "@" + "oracle.com");
                        insPs7.setInt(8, RandomGenerator.randomInteger(MIN_SALESID, MAX_SALESID));
                        insPs7.setDate(9, custSince);
                        insPs7.setString(10, "Ocasional");
                        insPs7.setString(11, "Music");
                        insPs7.setDate(12, dob);
                        insPs7.setString(13, "Y");
                        insPs7.setString(14, "N");
                        insPs7.setLong(15, addressID);
                        insPs7.setLong(16, custID);

                        insPs7.execute();
                        addInsertStatements(1);

                        insPs8.setLong(1, custID);
                        insPs8.setString(2, firstName);
                        insPs8.setString(3, lastName);
                        insPs8.setString(4, nls.language);
                        insPs8.setString(5, nls.territory);
                        insPs8.setInt(6,
                                      RandomGenerator.randomInteger(MIN_CREDITLIMIT, MAX_CREDITLIMIT));
                        insPs8.setString(7, firstName + "." + lastName + "@" + "oracle.com");
                        insPs8.setInt(8, RandomGenerator.randomInteger(MIN_SALESID, MAX_SALESID));
                        insPs8.setDate(9, custSince);
                        insPs8.setString(10, "Ocasional");
                        insPs8.setString(11, "Music");
                        insPs8.setDate(12, dob);
                        insPs8.setString(13, "Y");
                        insPs8.setString(14, "N");
                        insPs8.setLong(15, addressID);
                        insPs8.setLong(16, custID);

                        insPs8.execute();
                        addInsertStatements(1);

                        insPs9.setLong(1, custID);
                        insPs9.setString(2, firstName);
                        insPs9.setString(3, lastName);
                        insPs9.setString(4, nls.language);
                        insPs9.setString(5, nls.territory);
                        insPs9.setInt(6,
                                      RandomGenerator.randomInteger(MIN_CREDITLIMIT, MAX_CREDITLIMIT));
                        insPs9.setString(7, firstName + "." + lastName + "@" + "oracle.com");
                        insPs9.setInt(8, RandomGenerator.randomInteger(MIN_SALESID, MAX_SALESID));
                        insPs9.setDate(9, custSince);
                        insPs9.setString(10, "Ocasional");
                        insPs9.setString(11, "Music");
                        insPs9.setDate(12, dob);
                        insPs9.setString(13, "Y");
                        insPs9.setString(14, "N");
                        insPs9.setLong(15, addressID);
                        insPs9.setLong(16, custID);

                        insPs9.execute();
                        addInsertStatements(1);


                        insPs10.setLong(1, custID);
                        insPs10.setString(2, firstName);
                        insPs10.setString(3, lastName);
                        insPs10.setString(4, nls.language);
                        insPs10.setString(5, nls.territory);
                        insPs10.setInt(6,
                                      RandomGenerator.randomInteger(MIN_CREDITLIMIT, MAX_CREDITLIMIT));
                        insPs10.setString(7, firstName + "." + lastName + "@" + "oracle.com");
                        insPs10.setInt(8, RandomGenerator.randomInteger(MIN_SALESID, MAX_SALESID));
                        insPs10.setDate(9, custSince);
                        insPs10.setString(10, "Ocasional");
                        insPs10.setString(11, "Music");
                        insPs10.setDate(12, dob);
                        insPs10.setString(13, "Y");
                        insPs10.setString(14, "N");
                        insPs10.setLong(15, addressID);
                        insPs10.setLong(16, custID);

                        insPs10.execute();
                        addInsertStatements(1);


//                        insPs11.setLong(1, custID);
//                        insPs11.setString(2, firstName);
//                        insPs11.setString(3, lastName);
//                        insPs11.setString(4, nls.language);
//                        insPs11.setString(5, nls.territory);
//                        insPs11.setInt(6,
//                                      RandomGenerator.randomInteger(MIN_CREDITLIMIT, MAX_CREDITLIMIT));
//                        insPs11.setString(7, firstName + "." + lastName + "@" + "oracle.com");
//                        insPs11.setInt(8, RandomGenerator.randomInteger(MIN_SALESID, MAX_SALESID));
//                        insPs11.setDate(9, custSince);
//                        insPs11.setString(10, "Ocasional");
//                        insPs11.setString(11, "Music");
//                        insPs11.setDate(12, dob);
//                        insPs11.setString(13, "Y");
//                        insPs11.setString(14, "N");
//                        insPs11.setLong(15, addressID);
//                        insPs11.setLong(16, custID);
//
//                        insPs11.execute();
//                        addInsertStatements(1);
//
//                        insPs12.setLong(1, custID);
//                        insPs12.setString(2, firstName);
//                        insPs12.setString(3, lastName);
//                        insPs12.setString(4, nls.language);
//                        insPs12.setString(5, nls.territory);
//                        insPs12.setInt(6,
//                                      RandomGenerator.randomInteger(MIN_CREDITLIMIT, MAX_CREDITLIMIT));
//                        insPs12.setString(7, firstName + "." + lastName + "@" + "oracle.com");
//                        insPs12.setInt(8, RandomGenerator.randomInteger(MIN_SALESID, MAX_SALESID));
//                        insPs12.setDate(9, custSince);
//                        insPs12.setString(10, "Ocasional");
//                        insPs12.setString(11, "Music");
//                        insPs12.setDate(12, dob);
//                        insPs12.setString(13, "Y");
//                        insPs12.setString(14, "N");
//                        insPs12.setLong(15, addressID);
//                        insPs12.setLong(16, custID);
//
//                        insPs12.execute();
//                        addInsertStatements(1);
//
//                        insPs13.setLong(1, custID);
//                        insPs13.setString(2, firstName);
//                        insPs13.setString(3, lastName);
//                        insPs13.setString(4, nls.language);
//                        insPs13.setString(5, nls.territory);
//                        insPs13.setInt(6,
//                                      RandomGenerator.randomInteger(MIN_CREDITLIMIT, MAX_CREDITLIMIT));
//                        insPs13.setString(7, firstName + "." + lastName + "@" + "oracle.com");
//                        insPs13.setInt(8, RandomGenerator.randomInteger(MIN_SALESID, MAX_SALESID));
//                        insPs13.setDate(9, custSince);
//                        insPs13.setString(10, "Ocasional");
//                        insPs13.setString(11, "Music");
//                        insPs13.setDate(12, dob);
//                        insPs13.setString(13, "Y");
//                        insPs13.setString(14, "N");
//                        insPs13.setLong(15, addressID);
//                        insPs13.setLong(16, custID);
//
//                        insPs13.execute();
//                        addInsertStatements(1);
//
//                        insPs14.setLong(1, custID);
//                        insPs14.setString(2, firstName);
//                        insPs14.setString(3, lastName);
//                        insPs14.setString(4, nls.language);
//                        insPs14.setString(5, nls.territory);
//                        insPs14.setInt(6,
//                                      RandomGenerator.randomInteger(MIN_CREDITLIMIT, MAX_CREDITLIMIT));
//                        insPs14.setString(7, firstName + "." + lastName + "@" + "oracle.com");
//                        insPs14.setInt(8, RandomGenerator.randomInteger(MIN_SALESID, MAX_SALESID));
//                        insPs14.setDate(9, custSince);
//                        insPs14.setString(10, "Ocasional");
//                        insPs14.setString(11, "Music");
//                        insPs14.setDate(12, dob);
//                        insPs14.setString(13, "Y");
//                        insPs14.setString(14, "N");
//                        insPs14.setLong(15, addressID);
//                        insPs14.setLong(16, custID);
//
//                        insPs14.execute();
//                        addInsertStatements(1);
//
//                        insPs15.setLong(1, custID);
//                        insPs15.setString(2, firstName);
//                        insPs15.setString(3, lastName);
//                        insPs15.setString(4, nls.language);
//                        insPs15.setString(5, nls.territory);
//                        insPs15.setInt(6,
//                                      RandomGenerator.randomInteger(MIN_CREDITLIMIT, MAX_CREDITLIMIT));
//                        insPs15.setString(7, firstName + "." + lastName + "@" + "oracle.com");
//                        insPs15.setInt(8, RandomGenerator.randomInteger(MIN_SALESID, MAX_SALESID));
//                        insPs15.setDate(9, custSince);
//                        insPs15.setString(10, "Ocasional");
//                        insPs15.setString(11, "Music");
//                        insPs15.setDate(12, dob);
//                        insPs15.setString(13, "Y");
//                        insPs15.setString(14, "N");
//                        insPs15.setLong(15, addressID);
//                        insPs15.setLong(16, custID);
//
//                        insPs15.execute();
//                        addInsertStatements(1);

                    }



            connection.commit();
            addCommitStatements(1);
            thinkSleep();
            logon(connection, custID);
            addInsertStatements(1);
            addCommitStatements(1);
            getCustomerDetails(connection, custID);
            addSelectStatements(1);
            processTransactionEvent(new JdbcTaskEvent(this, getId(), (System.nanoTime() - executeStart), true, getInfoArray()));
        } catch (SQLException sbe) {
            logger.log(Level.FINE, String.format("Exception : %s", sbe.getMessage()));
            logger.log(Level.FINEST, "SQLException thrown : %s", sbe);
            try {
                addRollbackStatements(1);
                connection.rollback();
            } catch (SQLException er) {
                logger.log(Level.FINE, "Unable to rollback transaction");
            }
            processTransactionEvent(new JdbcTaskEvent(this, getId(), (System.nanoTime() - executeStart), false, getInfoArray()));
            throw new SwingBenchException(sbe);
        }
    }

    public void close() {
    }

    private class NLSSupport {

        String language = null;
        String territory = null;

    }
}
