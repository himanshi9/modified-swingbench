-- Suppress Warnings

drop sequence customer_seq;

drop sequence orders_seq;

drop sequence address_seq;

drop sequence address_seq1;

drop sequence address_seq2;

drop sequence address_seq3;

drop sequence address_seq4;

drop sequence address_seq5;

drop sequence address_seq6;

drop sequence address_seq7;

drop sequence address_seq8;

drop sequence address_seq9;

drop sequence address_seq10;

drop sequence logon_seq;

drop sequence card_details_seq;

-- End Suppress Warnings

begin
	declare
		cust_count	number :=0;
		order_count	number :=0;
		add_count	number :=0;
        add_count1	number :=0;
        add_count2	number :=0;
        add_count3	number :=0;
        add_count4	number :=0;
        add_count5	number :=0;
        add_count6	number :=0;
        add_count7	number :=0;
        add_count8	number :=0;
        add_count9	number :=0;
        add_count10	number :=0;
		logon_count	number :=0;
        card_count  number :=0;
	begin
		select nvl(max(customer_id),0) into cust_count from customers;
		select nvl(max(order_id),0) into order_count from orders;
		select nvl(max(address_id),0) into add_count from addresses;
        select nvl(max(address_id),0) into add_count1 from addresses1;
        select nvl(max(address_id),0) into add_count2 from addresses2;
        select nvl(max(address_id),0) into add_count3 from addresses3;
        select nvl(max(address_id),0) into add_count4 from addresses4;
        select nvl(max(address_id),0) into add_count5 from addresses5;
        select nvl(max(address_id),0) into add_count6 from addresses6;
        select nvl(max(address_id),0) into add_count7 from addresses7;
        select nvl(max(address_id),0) into add_count8 from addresses8;
        select nvl(max(address_id),0) into add_count9 from addresses9;
        select nvl(max(address_id),0) into add_count10 from addresses10;
		select nvl(max(logon_id),0) into logon_count from logon;
        select nvl(max(card_id),0) into card_count from card_details;
		cust_count := cust_count + 1;
		order_count := order_count + 1;
		add_count := add_count + 1;
        add_count1 := add_count1 + 1;
        add_count2 := add_count2 + 1;
        add_count3 := add_count3 + 1;
        add_count4 := add_count4 + 1;
        add_count5 := add_count5 + 1;
        add_count6 := add_count6 + 1;
        add_count7 := add_count7 + 1;
        add_count8 := add_count8 + 1;
        add_count9 := add_count9 + 1;
        add_count10 := add_count10 + 1;
		logon_count := logon_count + 1;
        card_count := card_count +1;
		execute immediate 'create sequence customer_seq start with '||cust_count||'  cache 10000000';
		execute immediate 'create sequence orders_seq start with '||order_count||' increment by 1 cache 10000000';
		execute immediate 'create sequence address_seq start with '||add_count||'  cache 10000000';
        execute immediate 'create sequence address_seq1 start with '||add_count1||'  cache 10000000';
        execute immediate 'create sequence address_seq2 start with '||add_count2||'  cache 10000000';
        execute immediate 'create sequence address_seq3 start with '||add_count3||'  cache 10000000';
        execute immediate 'create sequence address_seq4 start with '||add_count4||'  cache 10000000';
        execute immediate 'create sequence address_seq5 start with '||add_count5||'  cache 10000000';
        execute immediate 'create sequence address_seq6 start with '||add_count6||'  cache 10000000';
        execute immediate 'create sequence address_seq7 start with '||add_count7||'  cache 10000000';
        execute immediate 'create sequence address_seq8 start with '||add_count8||'  cache 10000000';
        execute immediate 'create sequence address_seq9 start with '||add_count9||'  cache 10000000';
        execute immediate 'create sequence address_seq10 start with '||add_count10||'  cache 10000000';
		execute immediate 'create sequence logon_seq start with '||logon_count||' cache 10000000';
        execute immediate 'create sequence card_details_seq start with '||card_count||' cache 10000000';
	end;
end;
/

-- End