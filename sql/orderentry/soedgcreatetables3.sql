-- Version 2 of the swingbench simple order entry benchmark schema
-- Includes additional columns to some tables and a new table addresses

CREATE TABLE customers
(
    customer_id                 NUMBER(12)
        CONSTRAINT cust_custid_nn NOT NULL,
    cust_first_name             VARCHAR2(40)
        CONSTRAINT cust_fname_nn NOT NULL,
    cust_last_name              VARCHAR2(40)
        CONSTRAINT cust_lname_nn NOT NULL,
    customer_title              varchar2(30),
    nls_language                VARCHAR2(3),
    nls_territory               VARCHAR2(90),
    region_id                   NUMBER(4)
        constraint cust_region_nn not null,
    credit_limit                NUMBER(9, 2),
    customer_email              VARCHAR2(100),
    customer_telephone_landline number(30),
    customer_telephone_mobile   number(30),
    coorporate_customer         VARCHAR(1),
    account_mgr_id              NUMBER(12),
    customer_since              DATE,
    customer_class              VARCHAR(40),
    suggestions                 VARCHAR(40),
    dob                         DATE,
    mailshot                    VARCHAR(1),
    partner_mailshot            VARCHAR(1),
    preferred_address           NUMBER(12),
    preferred_card              NUMBER(12)
) &compress initrans 16
    STORAGE (INITIAL 8M NEXT 8M);


CREATE TABLE addresses
(
    address_id       NUMBER(12)
        CONSTRAINT address_id_nn NOT NULL,
    customer_id      NUMBER(12)
        CONSTRAINT address_cust_id_nn NOT NULL,
    date_created     DATE
        CONSTRAINT address_datec_nn NOT NULL,
    region_id
        CONSTRAINT address_region_nn NOT NULL,
    house_no_or_name VARCHAR2(60),
    street_name      VARCHAR2(60),
    town             VARCHAR2(60),
    county           VARCHAR2(60),
    country          VARCHAR2(60),
    post_code        VARCHAR(12),
    zip_code         VARCHAR(12)
) &compress initrans 16
    STORAGE (INITIAL 8M NEXT 8M);



CREATE TABLE card_details
(
    card_id       NUMBER(12)
        CONSTRAINT card_id_nn NOT NULL,
    customer_id   NUMBER(12)
        CONSTRAINT card_cust_id_nn NOT NULL,
    card_type     VARCHAR2(60)
        CONSTRAINT card_type_nn NOT NULL,
    card_number   NUMBER(12)
        CONSTRAINT card_number_nn NOT NULL,
    expiry_date   DATE
        CONSTRAINT expiry_date_nn NOT NULL,
    is_valid      VARCHAR2(30)
        CONSTRAINT is_valid_nn NOT NULL,
    security_code NUMBER(6),
    region_id
        CONSTRAINT address_region_nn NOT NULL
) &compress initrans 16
  STORAGE (INITIAL 8M NEXT 8M);


CREATE TABLE warehouses
(
    warehouse_id   NUMBER(6),
    region_id      NUMBER(4),
    warehouse_name VARCHAR2(35),
    location_id    NUMBER(4)
) &compress;


CREATE TABLE order_items
(
    order_id           NUMBER(12)
        CONSTRAINT oi_order_id_nn NOT NULL,
    line_item_id       NUMBER(3)
        CONSTRAINT oi_lineitem_id_nn NOT NULL,
    product_id         NUMBER(6)
        CONSTRAINT oi_product_id_nn NOT NULL,
    unit_price         NUMBER(8, 2),
    quantity           NUMBER(8),
    dispatch_date      DATE,
    return_id          NUMBER(12),
    gift_wrap          VARCHAR(45),
    condition          VARCHAR(45),
    supplier_id        NUMBER(6),
    estimated_delivery DATE
) &compress initrans 16
    STORAGE (INITIAL 8M NEXT 8M);



CREATE TABLE orders
(
    order_id                NUMBER(12)
        CONSTRAINT order_order_id_nn NOT NULL,
    order_date              TIMESTAMP WITH LOCAL TIME ZONE
        CONSTRAINT order_date_nn NOT NULL,
    order_mode              VARCHAR2(8),
    customer_id             NUMBER(12)
        CONSTRAINT order_customer_id_nn NOT NULL,
    order_status            NUMBER(2),
    order_total             NUMBER(10, 2),
    sales_rep_id            NUMBER(6),
    promotion_id            NUMBER(6),
    warehouse_id            NUMBER(6),
    delivery_type           VARCHAR(60),
    cost_of_delivery        NUMBER(6),
    wait_till_all_available VARCHAR(60),
    delivery_address_id     NUMBER(12),
    customer_class          VARCHAR(60),
    card_id                 NUMBER(12),
    invoice_address_id      NUMBER(12),
    shipment_id             NUMBER(12),
    return_id               NUMBER(12)
) &compress initrans 16
    STORAGE (INITIAL 8M NEXT 8M);


CREATE TABLE returns
(
    return_id           number(12)
        CONSTRAINT re_return_id_nn NOT NULL,
    order_id            NUMBER(12)
        CONSTRAINT re_order_id_nn NOT NULL,
    line_item_id        NUMBER(3)
        CONSTRAINT re_lineitem_id_nn NOT NULL,
    return_date         DATE
        CONSTRAINT re_re_date_nn NOT NULL,
    reason_for_return   VARCHAR2(30)
        CONSTRAINT re_reason_nn NOT NULL,
    condition_of_return VARCHAR2(30)
        CONSTRAINT re_condition_nn NOT NULL,
    refund              VARCHAR2(1),
    replacement         VARCHAR2(1)
) &compress initrans 16
    STORAGE (INITIAL 8M NEXT 8M);

CREATE TABLE SHIPMENTS
(
    shippment_id number(12)
        CONSTRAINT sh_return_id_nn NOT NULL,
    order_id     NUMBER(12)
        CONSTRAINT sh_order_id_nn NOT NULL,
    line_item_id NUMBER(3)
        CONSTRAINT sh_lineitem_id_nn NOT NULL,
    shipment_date DATE
        CONSTRAINT sh_ship_date_nn NOT NULL,
    type_of_shipment
        CONSTRAINT sh_ship_type
            CHECK (type_of_shipment in ('next day', 'express', 'standard', 'economy')),
    carrier varchar2(100)
) &compress initrans 16
    STORAGE (INITIAL 8M NEXT 8M);


CREATE TABLE inventories
(
    product_id              NUMBER(6)
        CONSTRAINT inventory_prooduct_id_nn NOT NULL,
    warehouse_id            NUMBER(6)
        CONSTRAINT inventory_warehouse_id_nn NOT NULL,
    region_id               number(4)
        CONSTRAINT region_id_nn NOT NULL,
    warehouse_zone          NUMBER(8),
    warehouse_ailse         NUMBER(8),
    warehouse_shelf         NUMBER(4),
    warehouse_shelf_section NUMBER(4),
    quantity_on_hand        NUMBER(8)
        CONSTRAINT inventory_qoh_nn NOT NULL,
    reorder_threshold       NUMBER(8),
    last_refreshed          DATE,
    next_expected_delivery  DATE
) &compress initrans 16 pctfree 90 pctused 5;



CREATE TABLE product_information
(
    product_id          NUMBER(6)
        CONSTRAINT product_product_id_nn NOT NULL,
    product_name        VARCHAR2(50)
        CONSTRAINT product_product_name_nn NOT NULL,
    product_description VARCHAR2(2000),
    category_id         NUMBER(4)
        CONSTRAINT product_category_id_nn NOT NULL,
    weight_class        NUMBER(1),
    warranty_period     INTERVAL YEAR TO MONTH,
    supplier_id         NUMBER(6),
    product_status      VARCHAR2(20),
    list_price          NUMBER(8, 2),
    min_price           NUMBER(8, 2),
    catalog_url         VARCHAR2(50),
    CONSTRAINT product_status_lov
        CHECK (product_status in ('orderable', 'planned', 'under development', 'obsolete')
            )
) &compress;



create table logon
(
    logon_id    NUMBER
        CONSTRAINT logon_logon_id_nn NOT NULL,
    customer_id NUMBER
        CONSTRAINT logon_customer_id_nn NOT NULL,
    logon_date  DATE,
    region_id   number(4)
) &compress initrans 16
    STORAGE (INITIAL 8M NEXT 8M);


CREATE TABLE product_descriptions
(
    product_id             NUMBER(6),
    language_id            VARCHAR2(3),
    translated_name        NVARCHAR2(50)
        CONSTRAINT translated_name_nn NOT NULL,
    translated_description NVARCHAR2(2000)
        CONSTRAINT translated_desc_nn NOT NULL
) &compress;

create table regions
(
    region_id                  number(4),
    region_name                varchar2(100),
    region_head                varchar2(100),
    region_head_office_address varchar(200),
    region_telephone           varchar(20)
);

CREATE TABLE orderentry_metadata
(
    metadata_key   VARCHAR2(30),
    metadata_value VARCHAR2(30)
);


-- End;


