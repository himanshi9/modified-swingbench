
-- Version 2 of the swingbench simple order entry benchmark schema
-- Includes additional columns to some tables and a new table addresses

CREATE TABLE customers
(
    customer_id       NUMBER(12)
        CONSTRAINT cust_custid_nn NOT NULL,
    cust_first_name   VARCHAR2(40)
        CONSTRAINT cust_fname_nn NOT NULL,
    cust_last_name    VARCHAR2(40)
        CONSTRAINT cust_lname_nn NOT NULL,
    nls_language      VARCHAR2(3),
    nls_territory     VARCHAR2(90),
    credit_limit      NUMBER(9, 2),
    cust_email        VARCHAR2(100),
    account_mgr_id    NUMBER(12),
    customer_since    DATE,
    customer_class    VARCHAR(40),
    suggestions       VARCHAR(40),
    dob               DATE,
    mailshot          VARCHAR(1),
    partner_mailshot  VARCHAR(1),
    preferred_address NUMBER(12),
    preferred_card    NUMBER(12)
) &compress initrans 16
    STORAGE (INITIAL 8M NEXT 8M);

CREATE TABLE customers1
(
    customer_id       NUMBER(12)
        CONSTRAINT cust_custid_nnn NOT NULL,
    cust_first_name   VARCHAR2(40)
        CONSTRAINT cust_fname_nnn NOT NULL,
    cust_last_name    VARCHAR2(40)
        CONSTRAINT cust_lname_nnn NOT NULL,
    nls_language      VARCHAR2(3),
    nls_territory     VARCHAR2(90),
    credit_limit      NUMBER(9, 2),
    cust_email        VARCHAR2(100),
    account_mgr_id    NUMBER(12),
    customer_since    DATE,
    customer_class    VARCHAR(40),
    suggestions       VARCHAR(40),
    dob               DATE,
    mailshot          VARCHAR(1),
    partner_mailshot  VARCHAR(1),
    preferred_address NUMBER(12),
    preferred_card    NUMBER(12)
) &compress initrans 16
    STORAGE (INITIAL 8M NEXT 8M);

CREATE TABLE customers2
(
    customer_id       NUMBER(12)
        CONSTRAINT cust_custid_nn2 NOT NULL,
    cust_first_name   VARCHAR2(40)
        CONSTRAINT cust_fname_nn2 NOT NULL,
    cust_last_name    VARCHAR2(40)
        CONSTRAINT cust_lname_nn2 NOT NULL,
    nls_language      VARCHAR2(3),
    nls_territory     VARCHAR2(90),
    credit_limit      NUMBER(9, 2),
    cust_email        VARCHAR2(100),
    account_mgr_id    NUMBER(12),
    customer_since    DATE,
    customer_class    VARCHAR(40),
    suggestions       VARCHAR(40),
    dob               DATE,
    mailshot          VARCHAR(1),
    partner_mailshot  VARCHAR(1),
    preferred_address NUMBER(12),
    preferred_card    NUMBER(12)
) &compress initrans 16
    STORAGE (INITIAL 8M NEXT 8M);

CREATE TABLE customers3
(
    customer_id       NUMBER(12)
        CONSTRAINT cust_custid_nn3 NOT NULL,
    cust_first_name   VARCHAR2(40)
        CONSTRAINT cust_fname_nn3 NOT NULL,
    cust_last_name    VARCHAR2(40)
        CONSTRAINT cust_lname_nn3 NOT NULL,
    nls_language      VARCHAR2(3),
    nls_territory     VARCHAR2(90),
    credit_limit      NUMBER(9, 2),
    cust_email        VARCHAR2(100),
    account_mgr_id    NUMBER(12),
    customer_since    DATE,
    customer_class    VARCHAR(40),
    suggestions       VARCHAR(40),
    dob               DATE,
    mailshot          VARCHAR(1),
    partner_mailshot  VARCHAR(1),
    preferred_address NUMBER(12),
    preferred_card    NUMBER(12)
) &compress initrans 16
    STORAGE (INITIAL 8M NEXT 8M);

CREATE TABLE customers4
(
    customer_id       NUMBER(12)
        CONSTRAINT cust_custid_nn4 NOT NULL,
    cust_first_name   VARCHAR2(40)
        CONSTRAINT cust_fname_nn4 NOT NULL,
    cust_last_name    VARCHAR2(40)
        CONSTRAINT cust_lname_nn4 NOT NULL,
    nls_language      VARCHAR2(3),
    nls_territory     VARCHAR2(90),
    credit_limit      NUMBER(9, 2),
    cust_email        VARCHAR2(100),
    account_mgr_id    NUMBER(12),
    customer_since    DATE,
    customer_class    VARCHAR(40),
    suggestions       VARCHAR(40),
    dob               DATE,
    mailshot          VARCHAR(1),
    partner_mailshot  VARCHAR(1),
    preferred_address NUMBER(12),
    preferred_card    NUMBER(12)
) &compress initrans 16
    STORAGE (INITIAL 8M NEXT 8M);

CREATE TABLE customers5
(
    customer_id       NUMBER(12)
        CONSTRAINT cust_custid_nn5 NOT NULL,
    cust_first_name   VARCHAR2(40)
        CONSTRAINT cust_fname_nn5 NOT NULL,
    cust_last_name    VARCHAR2(40)
        CONSTRAINT cust_lname_nn5 NOT NULL,
    nls_language      VARCHAR2(3),
    nls_territory     VARCHAR2(90),
    credit_limit      NUMBER(9, 2),
    cust_email        VARCHAR2(100),
    account_mgr_id    NUMBER(12),
    customer_since    DATE,
    customer_class    VARCHAR(40),
    suggestions       VARCHAR(40),
    dob               DATE,
    mailshot          VARCHAR(1),
    partner_mailshot  VARCHAR(1),
    preferred_address NUMBER(12),
    preferred_card    NUMBER(12)
) &compress initrans 16
    STORAGE (INITIAL 8M NEXT 8M);

CREATE TABLE customers6
(
    customer_id       NUMBER(12)
        CONSTRAINT cust_custid_nn6 NOT NULL,
    cust_first_name   VARCHAR2(40)
        CONSTRAINT cust_fname_nn6 NOT NULL,
    cust_last_name    VARCHAR2(40)
        CONSTRAINT cust_lname_nn6 NOT NULL,
    nls_language      VARCHAR2(3),
    nls_territory     VARCHAR2(90),
    credit_limit      NUMBER(9, 2),
    cust_email        VARCHAR2(100),
    account_mgr_id    NUMBER(12),
    customer_since    DATE,
    customer_class    VARCHAR(40),
    suggestions       VARCHAR(40),
    dob               DATE,
    mailshot          VARCHAR(1),
    partner_mailshot  VARCHAR(1),
    preferred_address NUMBER(12),
    preferred_card    NUMBER(12)
) &compress initrans 16
    STORAGE (INITIAL 8M NEXT 8M);

CREATE TABLE customers7
(
    customer_id       NUMBER(12)
        CONSTRAINT cust_custid_nn7 NOT NULL,
    cust_first_name   VARCHAR2(40)
        CONSTRAINT cust_fname_nn7 NOT NULL,
    cust_last_name    VARCHAR2(40)
        CONSTRAINT cust_lname_nn7 NOT NULL,
    nls_language      VARCHAR2(3),
    nls_territory     VARCHAR2(90),
    credit_limit      NUMBER(9, 2),
    cust_email        VARCHAR2(100),
    account_mgr_id    NUMBER(12),
    customer_since    DATE,
    customer_class    VARCHAR(40),
    suggestions       VARCHAR(40),
    dob               DATE,
    mailshot          VARCHAR(1),
    partner_mailshot  VARCHAR(1),
    preferred_address NUMBER(12),
    preferred_card    NUMBER(12)
) &compress initrans 16
    STORAGE (INITIAL 8M NEXT 8M);

CREATE TABLE customers8
(
    customer_id       NUMBER(12)
        CONSTRAINT cust_custid_nn8 NOT NULL,
    cust_first_name   VARCHAR2(40)
        CONSTRAINT cust_fname_nn8 NOT NULL,
    cust_last_name    VARCHAR2(40)
        CONSTRAINT cust_lname_nn8 NOT NULL,
    nls_language      VARCHAR2(3),
    nls_territory     VARCHAR2(90),
    credit_limit      NUMBER(9, 2),
    cust_email        VARCHAR2(100),
    account_mgr_id    NUMBER(12),
    customer_since    DATE,
    customer_class    VARCHAR(40),
    suggestions       VARCHAR(40),
    dob               DATE,
    mailshot          VARCHAR(1),
    partner_mailshot  VARCHAR(1),
    preferred_address NUMBER(12),
    preferred_card    NUMBER(12)
) &compress initrans 16
    STORAGE (INITIAL 8M NEXT 8M);

CREATE TABLE customers9
(
    customer_id       NUMBER(12)
        CONSTRAINT cust_custid_nn9 NOT NULL,
    cust_first_name   VARCHAR2(40)
        CONSTRAINT cust_fname_nn9 NOT NULL,
    cust_last_name    VARCHAR2(40)
        CONSTRAINT cust_lname_nn9 NOT NULL,
    nls_language      VARCHAR2(3),
    nls_territory     VARCHAR2(90),
    credit_limit      NUMBER(9, 2),
    cust_email        VARCHAR2(100),
    account_mgr_id    NUMBER(12),
    customer_since    DATE,
    customer_class    VARCHAR(40),
    suggestions       VARCHAR(40),
    dob               DATE,
    mailshot          VARCHAR(1),
    partner_mailshot  VARCHAR(1),
    preferred_address NUMBER(12),
    preferred_card    NUMBER(12)
) &compress initrans 16
    STORAGE (INITIAL 8M NEXT 8M);

CREATE TABLE customers10
(
    customer_id       NUMBER(12)
        CONSTRAINT cust_custid_nn10 NOT NULL,
    cust_first_name   VARCHAR2(40)
        CONSTRAINT cust_fname_nn10 NOT NULL,
    cust_last_name    VARCHAR2(40)
        CONSTRAINT cust_lname_nn10 NOT NULL,
    nls_language      VARCHAR2(3),
    nls_territory     VARCHAR2(90),
    credit_limit      NUMBER(9, 2),
    cust_email        VARCHAR2(100),
    account_mgr_id    NUMBER(12),
    customer_since    DATE,
    customer_class    VARCHAR(40),
    suggestions       VARCHAR(40),
    dob               DATE,
    mailshot          VARCHAR(1),
    partner_mailshot  VARCHAR(1),
    preferred_address NUMBER(12),
    preferred_card    NUMBER(12)
) &compress initrans 16
    STORAGE (INITIAL 8M NEXT 8M);

--CREATE TABLE customers11
--(
--    customer_id       NUMBER(12)
--        CONSTRAINT cust_custid_nn11 NOT NULL,
--    cust_first_name   VARCHAR2(40)
--        CONSTRAINT cust_fname_nn11 NOT NULL,
--    cust_last_name    VARCHAR2(40)
--        CONSTRAINT cust_lname_nn11 NOT NULL,
--    nls_language      VARCHAR2(3),
--    nls_territory     VARCHAR2(90),
--    credit_limit      NUMBER(9, 2),
--    cust_email        VARCHAR2(100),
--    account_mgr_id    NUMBER(12),
--    customer_since    DATE,
--    customer_class    VARCHAR(40),
--    suggestions       VARCHAR(40),
--    dob               DATE,
--    mailshot          VARCHAR(1),
--    partner_mailshot  VARCHAR(1),
--    preferred_address NUMBER(12),
--    preferred_card    NUMBER(12)
--) &compress initrans 16
--    STORAGE (INITIAL 8M NEXT 8M);
--
--CREATE TABLE customers12
--(
--    customer_id       NUMBER(12)
--        CONSTRAINT cust_custid_nn12 NOT NULL,
--    cust_first_name   VARCHAR2(40)
--        CONSTRAINT cust_fname_nn12 NOT NULL,
--    cust_last_name    VARCHAR2(40)
--        CONSTRAINT cust_lname_nn12 NOT NULL,
--    nls_language      VARCHAR2(3),
--    nls_territory     VARCHAR2(90),
--    credit_limit      NUMBER(9, 2),
--    cust_email        VARCHAR2(100),
--    account_mgr_id    NUMBER(12),
--    customer_since    DATE,
--    customer_class    VARCHAR(40),
--    suggestions       VARCHAR(40),
--    dob               DATE,
--    mailshot          VARCHAR(1),
--    partner_mailshot  VARCHAR(1),
--    preferred_address NUMBER(12),
--    preferred_card    NUMBER(12)
--) &compress initrans 16
--    STORAGE (INITIAL 8M NEXT 8M);
--
--CREATE TABLE customers13
--(
--    customer_id       NUMBER(12)
--        CONSTRAINT cust_custid_nn13 NOT NULL,
--    cust_first_name   VARCHAR2(40)
--        CONSTRAINT cust_fname_nn13 NOT NULL,
--    cust_last_name    VARCHAR2(40)
--        CONSTRAINT cust_lname_nn13 NOT NULL,
--    nls_language      VARCHAR2(3),
--    nls_territory     VARCHAR2(90),
--    credit_limit      NUMBER(9, 2),
--    cust_email        VARCHAR2(100),
--    account_mgr_id    NUMBER(12),
--    customer_since    DATE,
--    customer_class    VARCHAR(40),
--    suggestions       VARCHAR(40),
--    dob               DATE,
--    mailshot          VARCHAR(1),
--    partner_mailshot  VARCHAR(1),
--    preferred_address NUMBER(12),
--    preferred_card    NUMBER(12)
--) &compress initrans 16
--    STORAGE (INITIAL 8M NEXT 8M);
--
--CREATE TABLE customers14
--(
--    customer_id       NUMBER(12)
--        CONSTRAINT cust_custid_nn14 NOT NULL,
--    cust_first_name   VARCHAR2(40)
--        CONSTRAINT cust_fname_nn14 NOT NULL,
--    cust_last_name    VARCHAR2(40)
--        CONSTRAINT cust_lname_nn14 NOT NULL,
--    nls_language      VARCHAR2(3),
--    nls_territory     VARCHAR2(90),
--    credit_limit      NUMBER(9, 2),
--    cust_email        VARCHAR2(100),
--    account_mgr_id    NUMBER(12),
--    customer_since    DATE,
--    customer_class    VARCHAR(40),
--    suggestions       VARCHAR(40),
--    dob               DATE,
--    mailshot          VARCHAR(1),
--    partner_mailshot  VARCHAR(1),
--    preferred_address NUMBER(12),
--    preferred_card    NUMBER(12)
--) &compress initrans 16
--    STORAGE (INITIAL 8M NEXT 8M);
--
--CREATE TABLE customers15
--(
--    customer_id       NUMBER(12)
--        CONSTRAINT cust_custid_nn15 NOT NULL,
--    cust_first_name   VARCHAR2(40)
--        CONSTRAINT cust_fname_nn15 NOT NULL,
--    cust_last_name    VARCHAR2(40)
--        CONSTRAINT cust_lname_nn15 NOT NULL,
--    nls_language      VARCHAR2(3),
--    nls_territory     VARCHAR2(90),
--    credit_limit      NUMBER(9, 2),
--    cust_email        VARCHAR2(100),
--    account_mgr_id    NUMBER(12),
--    customer_since    DATE,
--    customer_class    VARCHAR(40),
--    suggestions       VARCHAR(40),
--    dob               DATE,
--    mailshot          VARCHAR(1),
--    partner_mailshot  VARCHAR(1),
--    preferred_address NUMBER(12),
--    preferred_card    NUMBER(12)
--) &compress initrans 16
--    STORAGE (INITIAL 8M NEXT 8M);


CREATE TABLE addresses
  ( address_id            NUMBER(12)  CONSTRAINT address_id_nn NOT NULL,
  	customer_id           NUMBER(12)  CONSTRAINT address_cust_id_nn NOT NULL,
    date_created          DATE CONSTRAINT address_datec_nn NOT NULL,
    house_no_or_name      VARCHAR2(60),
    street_name           VARCHAR2(60),
    town                  VARCHAR2(60),
    county                VARCHAR2(60),
    country               VARCHAR2(60),
    post_code             VARCHAR(12),
    zip_code              VARCHAR(12)
    ) &compress initrans 16
    STORAGE (INITIAL 8M NEXT 8M);

CREATE TABLE addresses1
  ( address_id            NUMBER(12)  CONSTRAINT address_id_nn1 NOT NULL,
    customer_id           NUMBER(12)  CONSTRAINT address_cust_id_nn1 NOT NULL,
    date_created          DATE CONSTRAINT address_datec_nn1 NOT NULL,
    house_no_or_name      VARCHAR2(60),
    street_name           VARCHAR2(60),
    town                  VARCHAR2(60),
    county                VARCHAR2(60),
    country               VARCHAR2(60),
    post_code             VARCHAR(12),
    zip_code              VARCHAR(12)
    ) &compress initrans 16
    STORAGE (INITIAL 8M NEXT 8M);

CREATE TABLE addresses2
  ( address_id            NUMBER(12)  CONSTRAINT address_id_nn2 NOT NULL,
    customer_id           NUMBER(12)  CONSTRAINT address_cust_id_nn2 NOT NULL,
    date_created          DATE CONSTRAINT address_datec_nn2 NOT NULL,
    house_no_or_name      VARCHAR2(60),
    street_name           VARCHAR2(60),
    town                  VARCHAR2(60),
    county                VARCHAR2(60),
    country               VARCHAR2(60),
    post_code             VARCHAR(12),
    zip_code              VARCHAR(12)
    ) &compress initrans 16
    STORAGE (INITIAL 8M NEXT 8M);

CREATE TABLE addresses3
  ( address_id            NUMBER(12)  CONSTRAINT address_id_nn3 NOT NULL,
    customer_id           NUMBER(12)  CONSTRAINT address_cust_id_nn3 NOT NULL,
    date_created          DATE CONSTRAINT address_datec_nn3 NOT NULL,
    house_no_or_name      VARCHAR2(60),
    street_name           VARCHAR2(60),
    town                  VARCHAR2(60),
    county                VARCHAR2(60),
    country               VARCHAR2(60),
    post_code             VARCHAR(12),
    zip_code              VARCHAR(12)
    ) &compress initrans 16
    STORAGE (INITIAL 8M NEXT 8M);

CREATE TABLE addresses4
  ( address_id            NUMBER(12)  CONSTRAINT address_id_nn4 NOT NULL,
    customer_id           NUMBER(12)  CONSTRAINT address_cust_id_nn4 NOT NULL,
    date_created          DATE CONSTRAINT address_datec_nn4 NOT NULL,
    house_no_or_name      VARCHAR2(60),
    street_name           VARCHAR2(60),
    town                  VARCHAR2(60),
    county                VARCHAR2(60),
    country               VARCHAR2(60),
    post_code             VARCHAR(12),
    zip_code              VARCHAR(12)
    ) &compress initrans 16
    STORAGE (INITIAL 8M NEXT 8M);

CREATE TABLE addresses5
  ( address_id            NUMBER(12)  CONSTRAINT address_id_nn5 NOT NULL,
    customer_id           NUMBER(12)  CONSTRAINT address_cust_id_nn5 NOT NULL,
    date_created          DATE CONSTRAINT address_datec_nn5 NOT NULL,
    house_no_or_name      VARCHAR2(60),
    street_name           VARCHAR2(60),
    town                  VARCHAR2(60),
    county                VARCHAR2(60),
    country               VARCHAR2(60),
    post_code             VARCHAR(12),
    zip_code              VARCHAR(12)
    ) &compress initrans 16
    STORAGE (INITIAL 8M NEXT 8M);

CREATE TABLE addresses6
  ( address_id            NUMBER(12)  CONSTRAINT address_id_nn6 NOT NULL,
    customer_id           NUMBER(12)  CONSTRAINT address_cust_id_nn6 NOT NULL,
    date_created          DATE CONSTRAINT address_datec_nn6 NOT NULL,
    house_no_or_name      VARCHAR2(60),
    street_name           VARCHAR2(60),
    town                  VARCHAR2(60),
    county                VARCHAR2(60),
    country               VARCHAR2(60),
    post_code             VARCHAR(12),
    zip_code              VARCHAR(12)
    ) &compress initrans 16
    STORAGE (INITIAL 8M NEXT 8M);

CREATE TABLE addresses7
  ( address_id            NUMBER(12)  CONSTRAINT address_id_nn7 NOT NULL,
    customer_id           NUMBER(12)  CONSTRAINT address_cust_id_nn7 NOT NULL,
    date_created          DATE CONSTRAINT address_datec_nn7 NOT NULL,
    house_no_or_name      VARCHAR2(60),
    street_name           VARCHAR2(60),
    town                  VARCHAR2(60),
    county                VARCHAR2(60),
    country               VARCHAR2(60),
    post_code             VARCHAR(12),
    zip_code              VARCHAR(12)
    ) &compress initrans 16
    STORAGE (INITIAL 8M NEXT 8M);

CREATE TABLE addresses8
  ( address_id            NUMBER(12)  CONSTRAINT address_id_nn8 NOT NULL,
    customer_id           NUMBER(12)  CONSTRAINT address_cust_id_nn8 NOT NULL,
    date_created          DATE CONSTRAINT address_datec_nn8 NOT NULL,
    house_no_or_name      VARCHAR2(60),
    street_name           VARCHAR2(60),
    town                  VARCHAR2(60),
    county                VARCHAR2(60),
    country               VARCHAR2(60),
    post_code             VARCHAR(12),
    zip_code              VARCHAR(12)
    ) &compress initrans 16
    STORAGE (INITIAL 8M NEXT 8M);

CREATE TABLE addresses9
  ( address_id            NUMBER(12)  CONSTRAINT address_id_nn9 NOT NULL,
    customer_id           NUMBER(12)  CONSTRAINT address_cust_id_nn9 NOT NULL,
    date_created          DATE CONSTRAINT address_datec_nn9 NOT NULL,
    house_no_or_name      VARCHAR2(60),
    street_name           VARCHAR2(60),
    town                  VARCHAR2(60),
    county                VARCHAR2(60),
    country               VARCHAR2(60),
    post_code             VARCHAR(12),
    zip_code              VARCHAR(12)
    ) &compress initrans 16
    STORAGE (INITIAL 8M NEXT 8M);

CREATE TABLE addresses10
  ( address_id            NUMBER(12)  CONSTRAINT address_id_nn10 NOT NULL,
    customer_id           NUMBER(12)  CONSTRAINT address_cust_id_nn10 NOT NULL,
    date_created          DATE CONSTRAINT address_datec_nn10 NOT NULL,
    house_no_or_name      VARCHAR2(60),
    street_name           VARCHAR2(60),
    town                  VARCHAR2(60),
    county                VARCHAR2(60),
    country               VARCHAR2(60),
    post_code             VARCHAR(12),
    zip_code              VARCHAR(12)
    ) &compress initrans 16
    STORAGE (INITIAL 8M NEXT 8M);

CREATE TABLE card_details
(
    card_id       NUMBER(12)
        CONSTRAINT card_id_nn NOT NULL,
    customer_id   NUMBER(12)
        CONSTRAINT card_cust_id_nn NOT NULL,
    card_type     VARCHAR2(60)
        CONSTRAINT card_type_nn NOT NULL,
    card_number   NUMBER(12)
        CONSTRAINT card_number_nn NOT NULL,
    expiry_date   DATE
        CONSTRAINT expiry_date_nn NOT NULL,
    is_valid      VARCHAR2(30)
        CONSTRAINT is_valid_nn NOT NULL,
    security_code NUMBER(6)
) &compress initrans 16
  STORAGE (INITIAL 8M NEXT 8M);



CREATE TABLE warehouses
    ( warehouse_id          NUMBER(6) 
    , warehouse_name        VARCHAR2(35)
    , location_id           NUMBER(4)
    ) &compress;



CREATE TABLE order_items
(
    order_id           NUMBER(12)
        CONSTRAINT oi_order_id_nn NOT NULL,
    line_item_id       NUMBER(3)
        CONSTRAINT oi_lineitem_id_nn NOT NULL,
    product_id         NUMBER(6)
        CONSTRAINT oi_product_id_nn NOT NULL,
    unit_price         NUMBER(8, 2),
    quantity           NUMBER(8),
    dispatch_date      DATE,
    return_date        DATE,
    gift_wrap          VARCHAR(45),
    condition          VARCHAR(45),
    supplier_id        NUMBER(6),
    estimated_delivery DATE
) &compress initrans 16
    STORAGE (INITIAL 8M NEXT 8M);
    

CREATE TABLE orders
(
    order_id                NUMBER(12)
        CONSTRAINT order_order_id_nn NOT NULL,
    order_date              TIMESTAMP WITH LOCAL TIME ZONE
        CONSTRAINT order_date_nn NOT NULL,
    order_mode              VARCHAR2(8),
    customer_id             NUMBER(12)
        CONSTRAINT order_customer_id_nn NOT NULL,
    order_status            NUMBER(2),
    order_total             NUMBER(10, 2),
    sales_rep_id            NUMBER(6),
    promotion_id            NUMBER(6),
    warehouse_id            NUMBER(6),
    delivery_type           VARCHAR(60),
    cost_of_delivery        NUMBER(6),
    wait_till_all_available VARCHAR(60),
    delivery_address_id     NUMBER(12),
    customer_class          VARCHAR(60),
    card_id                 NUMBER(12),
    invoice_address_id      NUMBER(12)
) &compress initrans 16
    STORAGE (INITIAL 8M NEXT 8M);   


CREATE TABLE inventories
   ( product_id             NUMBER(6) CONSTRAINT inventory_prooduct_id_nn NOT NULL
   , warehouse_id           NUMBER(6) CONSTRAINT inventory_warehouse_id_nn NOT NULL
   , quantity_on_hand       NUMBER(8) CONSTRAINT inventory_qoh_nn NOT NULL
   ) &compress initrans 16 pctfree 90 pctused 5;



CREATE TABLE product_information
    ( product_id            NUMBER(6) CONSTRAINT product_product_id_nn NOT NULL
    , product_name          VARCHAR2(50) CONSTRAINT product_product_name_nn NOT NULL
    , product_description   VARCHAR2(2000)
    , category_id           NUMBER(4) CONSTRAINT product_category_id_nn NOT NULL
    , weight_class          NUMBER(1)
    , warranty_period       INTERVAL YEAR TO MONTH
    , supplier_id           NUMBER(6)
    , product_status        VARCHAR2(20)
    , list_price            NUMBER(8,2)
    , min_price             NUMBER(8,2)
    , catalog_url           VARCHAR2(50)
    , CONSTRAINT            product_status_lov
                            CHECK (product_status in ('orderable'
                                                  ,'planned'
                                                  ,'under development'
                                                  ,'obsolete')
                               )
    ) &compress;



create table logon
    (logon_id				NUMBER CONSTRAINT logon_logon_id_nn NOT NULL,
    customer_id	          	NUMBER CONSTRAINT logon_customer_id_nn NOT NULL,
    logon_date              DATE
    ) &compress initrans 16
    STORAGE (INITIAL 8M NEXT 8M);



CREATE TABLE product_descriptions
    ( product_id             NUMBER(6)
    , language_id            VARCHAR2(3)
    , translated_name        NVARCHAR2(50) CONSTRAINT translated_name_nn NOT NULL
    , translated_description NVARCHAR2(2000) CONSTRAINT translated_desc_nn NOT NULL
    ) &compress;



CREATE TABLE orderentry_metadata
  (
    metadata_key            VARCHAR2(30),
    metadata_value          VARCHAR2(30)
  );



-- End;


