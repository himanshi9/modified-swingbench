drop table nation cascade constraints;

drop table region cascade constraints;

drop table part cascade constraints;

drop table supplier cascade constraints;

drop table partsupp cascade constraints;

drop table customer cascade constraints;

drop table orders cascade constraints;

drop table lineitem cascade constraints;

--End