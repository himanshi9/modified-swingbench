alter session enable shard ddl;
@sharded_create_tables.sql;
@sharded_sequences.sql;
alter session disable shard ddl;
@sharded_product_descriptions.sql;
@sharded_product_information.sql;
commit;
alter session enable shard ddl;
@sharded_views.sql;
commit;




