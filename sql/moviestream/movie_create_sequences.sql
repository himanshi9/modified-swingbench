-- Create Movie Stream Sequences

-- Suppress Warnings

drop sequence CUST_CONTACT_SEQ;

drop sequence CUST_DEVICE_SEQ;

drop sequence CUST_FF_SEQ;

drop sequence CUST_PAYMENT_INFO_SEQ;

drop sequence CUST_REVIEW_SEQ;

drop sequence CUST_SALES_TX_SEQ;

drop sequence CUST_INTERACTION_SEQ;

-- End Suppress Warnings

begin
    declare
        cust_count        number := 0;
        device_count      number := 0;
        ff_count          number := 0;
        pay_count         number := 0;
        review_count      number := 0;
        sales_count       number := 0;
        interaction_count number := 0;
    begin
        select nvl(max(cust_id), 0) into cust_count from CUSTOMER_CONTACT;
        select nvl(max(DEVICE_ID), 0) into device_count from CUSTOMER_DEVICE;
        select nvl(max(FRIEND_FAMILY_ID), 0) into ff_count from CUSTOMER_FRIENDS_AND_FAMILY;
        select nvl(max(PAYMENT_INFORMATION_ID), 0) into pay_count from CUSTOMER_PAYMENT_INFORMATION;
        select nvl(max(SALES_TRANSACTION_ID), 0) into sales_count from CUSTOMER_SALES_TRANSACTION;
        select nvl(max(REVIEW_ID), 0) into review_count from CUSTOMER_REVIEW;
        select nvl(max(INTERACTION_ID), 0) into interaction_count from INTERACTION;
        execute immediate 'create sequence CUST_CONTACT_SEQ start with ' || (cust_count + 1) || '  cache 100000';
        execute immediate 'create sequence CUST_DEVICE_SEQ start with ' || (device_count + 1) || ' increment by 1 cache 100000';
        execute immediate 'create sequence CUST_FF_SEQ start with ' || (ff_count + 1) || '  cache 100000';
        execute immediate 'create sequence CUST_PAYMENT_INFO_SEQ start with ' || (pay_count + 1) || ' cache 100000';
        execute immediate 'create sequence CUST_SALES_TX_SEQ start with ' || (sales_count + 1) || ' cache 100000';
        execute immediate 'create sequence CUST_REVIEW_SEQ start with ' || (review_count + 1) || ' cache 100000';
        execute immediate 'create sequence CUST_INTERACTION_SEQ start with ' || (interaction_count + 1) || ' cache 100000';
    end;
end;
/

