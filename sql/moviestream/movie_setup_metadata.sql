-- Populate Movie Streams Meta Data

BEGIN
    DECLARE
        max_customer_id NUMBER := 1000000;
        min_customer_id NUMBER := 1;
        max_sales_id    NUMBER := 1000000;
        min_sales_id    NUMBER := 1;
    begin
        execute immediate 'truncate table MOVIE_METADATA';
        SELECT MIN(SALES_TRANSACTION_ID), MAX(SALES_TRANSACTION_ID)
        INTO min_sales_id     , max_sales_id
        from CUSTOMER_SALES_TRANSACTION;
        INSERT INTO MOVIE_METADATA
        (
            metadata_key, metadata_value
        )
        VALUES
            (
                'MIN_SALES_TRANSACTION_ID', to_char(min_sales_id)
            );
        INSERT INTO MOVIE_METADATA
        (
            metadata_key, metadata_value
        )
        VALUES
            (
                'MAX_SALES_TRANSACTION_ID', to_char(max_sales_id)
            );
        commit;
        SELECT MIN(CUST_ID), MAX(CUST_ID)
        INTO min_customer_Id        , max_customer_id
        from CUSTOMER_CONTACT;
        INSERT INTO MOVIE_METADATA
        (
            metadata_key, metadata_value
        )
        VALUES
            (
                'MIN_CUST_ID', to_char(min_customer_Id)
            );
        INSERT INTO MOVIE_METADATA
        (
            metadata_key, metadata_value
        )
        VALUES
            (
                'MAX_CUST_ID', to_char(max_customer_id)
            );
        commit;
    END;
END;

