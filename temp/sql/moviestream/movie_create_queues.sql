-- Create Event Queues

-- Suppress Warnings

begin
    dbms_aqadm.STOP_QUEUE(queue_name => 'interaction_event');
    DBMS_AQADM.DROP_QUEUE('interaction_event');
end;

begin
    dbms_aqadm.STOP_QUEUE(queue_name => 'purchase_event');
    DBMS_AQADM.DROP_QUEUE('purchase_event');
end;

-- End Suppress Warnings
begin
    declare
        subscriber sys.aq$_agent;
    begin
        dbms_aqadm.create_sharded_queue(
            -- note, in Oracle 19c this is called create_sharded_queue() but has the same parameters
                queue_name => 'interaction_event',
                multiple_consumers => true
            );
        dbms_aqadm.start_queue(
                queue_name => 'interaction_event'
            );
        dbms_aqadm.add_subscriber(
                queue_name => 'interaction_event',
                subscriber => sys.aq$_agent(
                        'interaction_event_subscriber', -- the subscriber name
                        null, -- address, only used for notifications
                        0 -- protocol
                    ),
                rule => 'correlation = ''interaction_event_subscriber'''
            );
    end;
end;

begin
    declare
        subscriber sys.aq$_agent;
    begin
        dbms_aqadm.create_sharded_queue(
                queue_name => 'purchase_event',
                multiple_consumers => true
            );
        dbms_aqadm.start_queue(
                queue_name => 'purchase_event'
            );
        dbms_aqadm.add_subscriber(
                queue_name => 'purchase_event',
                subscriber => sys.aq$_agent(
                        'purchase_event_subscriber', -- the subscriber name
                        null, -- address, only used for notifications
                        0 -- protocol
                    ),
                rule => 'correlation = ''purchase_event_subscriber'''
            );
    end;
end;



