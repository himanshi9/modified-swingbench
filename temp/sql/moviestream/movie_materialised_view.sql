create materialized view MOVIE
    refresh complete on demand
as
WITH GE as (select GM.MOVIE_ID, JSON_ARRAYAGG(G.GENERE_NAME ORDER BY G.GENERE_NAME) AS GENERE
            from MOVIES_GENERE_MAP GM,
                 GENERES G
            where G.GENERE_ID = GM.GENERE_ID
            GROUP BY GM.MOVIE_ID)
   , CA as (select GM.MOVIE_ID, JSON_ARRAYAGG(C.CAST_NAME ORDER BY C.CAST_NAME) AS CAST
            from MOVIES_CAST_MAP GM,
                 CAST C
            where C.CAST_ID = GM.CAST_ID
            GROUP BY GM.MOVIE_ID)
   , AW as (select GM.MOVIE_ID, JSON_ARRAYAGG(C.AWARD_NAME ORDER BY C.AWARD_NAME) AS AWARDS
            from MOVIES_AWARD_MAP GM,
                 AWARDS C
            where C.AWARD_ID = GM.AWARDS_ID
            GROUP BY GM.MOVIE_ID)
   , NO as (select GM.MOVIE_ID, JSON_ARRAYAGG(C.NOMINATION_NAME ORDER BY C.NOMINATION_NAME) AS NOMINATIONS
            from MOVIES_NOMINATION_MAP GM,
                 NOMINATIONS C
            where C.NOMINATION_ID = GM.NOMINATION_ID
            GROUP BY GM.MOVIE_ID)
   , ST as (select GM.MOVIE_ID, JSON_ARRAYAGG(C.STUDIO_NAME ORDER BY C.STUDIO_NAME) AS STUDIOS
            from MOVIES_STUDIO_MAP GM,
                 STUDIOS C
            where C.STUDIO_ID = GM.STUDIO_ID
            GROUP BY GM.MOVIE_ID)
   , CR as (select MOVIE_ID, JSON_ARRAYAGG(crew) as CREW
            from (select GM.MOVIE_ID                                                                     as movie_id,
                         JSON_OBJECT('job' VALUE GM.CREW_ROLE, 'names' VALUE json_arrayagg(G.CREW_NAME)) as crew
                  from MOVIES_CREW_MAP GM,
                       CREW G
                  where G.CREW_ID = GM.CREW_ID
                  GROUP BY GM.MOVIE_ID, GM.CREW_ROLE)
            group by MOVIE_ID)
select M.MOVIE_ID,
       M.TITLE,
       M.BUDGET,
       M.GROSS,
       M.LIST_PRICE,
       GE.GENERE  AS GENRES,
       M.SKU,
       M.YEAR,
       M.OPENING_DATE,
       M.VIEWS,
       CA.CAST,
       CR.CREW,
       ST.STUDIOS AS STUDIO,
       M.MAIN_SUBJECT,
       AW.AWARDS,
       NO.NOMINATIONS,
       M.RUNTIME,
       M.SUMMARY
from MOVIE_DETAILS M,
     GE,
     CA,
     AW,
     NO,
     ST,
     CR
where GE.MOVIE_ID(+) = M.MOVIE_ID
  and CA.MOVIE_ID(+) = M.MOVIE_ID
  and NO.MOVIE_ID(+) = M.MOVIE_ID
  and ST.MOVIE_ID(+) = M.MOVIE_ID
  and AW.MOVIE_ID(+) = M.MOVIE_ID
  and CR.MOVIE_ID(+) = M.MOVIE_ID;

