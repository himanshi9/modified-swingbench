-- Create Movie Stream Tables

create table CUSTOMER_CONTACT
(
    CUST_ID            NUMBER not null,
    LAST_NAME          VARCHAR2(200),
    FIRST_NAME         VARCHAR2(200),
    EMAIL              VARCHAR2(500),
    STREET_ADDRESS     VARCHAR2(400),
    POSTAL_CODE        VARCHAR2(10),
    CITY               VARCHAR2(100),
    STATE_PROVINCE     VARCHAR2(100),
    COUNTRY            VARCHAR2(400),
    COUNTRY_CODE       VARCHAR2(2),
    CONTINENT          VARCHAR2(400),
    YRS_CUSTOMER       NUMBER,
    PROMOTION_RESPONSE NUMBER,
    LOC_LAT            NUMBER,
    LOC_LONG           NUMBER
);

create table CUSTOMER_DEVICE
(
    DEVICE_ID           NUMBER        not null,
    CUST_ID             NUMBER        not null,
    DEVICE_TYPE         VARCHAR2(100) not null,
    DEVICE_MANUFACTURER VARCHAR2(200),
    DEVICE_OS           VARCHAR2(200),
    DEVICE_VERSION      VARCHAR2(200),
    INITIAL_ACCESS      TIMESTAMP,
    LAST_ACCESS         TIMESTAMP,
    ACCESS_COUNT        NUMBER
);

create table CUSTOMER_FRIENDS_AND_FAMILY
(
    FRIEND_FAMILY_ID NUMBER not null,
    CUSTOMER_SOURCE  NUMBER,
    RELATIONSHIP     VARCHAR2(100),
    CUSTOMER_TARGET  NUMBER
);

create table CUSTOMER_PAYMENT_INFORMATION
(
    PAYMENT_INFORMATION_ID NUMBER        not null,
    CUST_ID                NUMBER        not null,
    CARD_NUMBER            VARCHAR2(30)  not null,
    PROVIDER               VARCHAR2(100) not null,
    BILLING_STREET_ADDRESS VARCHAR2(400),
    BILLING_POSTAL_CODE    VARCHAR2(10),
    BILLING_CITY           VARCHAR2(100),
    BILLING_STATE_COUNTY   VARCHAR2(100),
    BILLING_COUNTRY        VARCHAR2(400),
    BILLING_COUNTRY_CODE   VARCHAR2(2),
    EXPIRY_DATE            DATE          not null,
    DEFAULT_PAYMENT_METHOD CHAR          not null
);

create table CUSTOMER_REVIEW
(
    CUST_ID          NUMBER       not null,
    MOVIE_ID         NUMBER       not null,
    STAR_RATING      NUMBER       not null,
    REVIEW           VARCHAR2(4000),
    REVIEW_ID        NUMBER       not null,
    REVIEW_TIMESTAMP TIMESTAMP(6) not null
);

create table CUSTOMER_SALES_TRANSACTION
(
    SALES_TRANSACTION_ID            NUMBER       not null,
    CUST_ID                         NUMBER       not null,
    MOVIE_ID                        NUMBER       not null,
    TRANSACTION_TIMESTAMP           TIMESTAMP(6) not null,
    DEVICE_ID                       NUMBER       not null,
    PAYMENT_METHOD                  NUMBER       not null,
    CUSTOMER_PAYMENT_INFORMATION_ID NUMBER,
    PROMOTION_DISCOUNT              VARCHAR2(100),
    PAYMENT_AMOUNT                  NUMBER       not null
);

create table INTERACTION
(
    INTERACTION_ID     NUMBER not null,
    CUST_ID            NUMBER not null,
    INTERACTION_TYPE   VARCHAR2(20) not null,
    INTERACTION_TIME   TIMESTAMP not null
);

CREATE TABLE movie_metadata
( METADATA_KEY         VARCHAR2(30),
  METADATA_VALUE       VARCHAR2(30)
);

create table MOVIE_DETAILS
(
    MOVIE_ID     NUMBER not null constraint MOVIE_DETAILS_PK primary key,
    TITLE        VARCHAR2(200),
    BUDGET       NUMBER,
    GROSS        NUMBER,
    LIST_PRICE   NUMBER,
    SKU          VARCHAR2(30),
    YEAR         NUMBER,
    OPENING_DATE DATE,
    VIEWS        NUMBER,
    MAIN_SUBJECT VARCHAR2(4000),
    RUNTIME      NUMBER,
    SUMMARY      VARCHAR2(4000)
);

create table CAST
(
    CAST_ID NUMBER not null constraint CAST_PK primary key,
    CAST_NAME VARCHAR2(200),
    CAST_DOB DATE
);

create table STUDIOS
(
    STUDIO_ID NUMBER not null constraint STUDIOS_PK primary key,
    STUDIO_NAME VARCHAR2(200)
);

create table AWARDS
(
    AWARD_ID NUMBER not null constraint AWARDS_PK primary key,
    AWARD_NAME VARCHAR2(200)
);

create table NOMINATIONS
(
    NOMINATION_ID NUMBER not null constraint NOMINATIONS_PK primary key,
    NOMINATION_NAME VARCHAR2(200)
);

create table MOVIES_CAST_MAP
(
    MOVIE_ID NUMBER,
    CAST_ID NUMBER
);

create table MOVIES_AWARD_MAP
(
    MOVIE_ID NUMBER,
    AWARDS_ID NUMBER
);

create table GENERES
(
    GENERE_ID NUMBER not null constraint GENERE_PK primary key,
    GENERE_NAME VARCHAR2(200),
    GENERE_DESCRIPTION VARCHAR2(200)
);

create table CREW
(
    CREW_ID NUMBER not null constraint CREW_PK primary key,
    CREW_NAME VARCHAR2(200)
);

create table MOVIES_GENERE_MAP
(
    MOVIE_ID NUMBER,
    GENERE_ID NUMBER
);

create table MOVIES_CREW_MAP
(
    MOVIE_ID NUMBER,
    CREW_ID NUMBER,
    CREW_ROLE VARCHAR2(200)
);

create table MOVIES_STUDIO_MAP
(
    MOVIE_ID NUMBER,
    STUDIO_ID NUMBER
);

create table MOVIES_NOMINATION_MAP
(
    MOVIE_ID NUMBER,
    NOMINATION_ID NUMBER
);


