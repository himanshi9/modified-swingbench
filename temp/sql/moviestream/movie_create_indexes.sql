-- Create Movie Stream Indexes

-- Suppress Warnings

begin
    CTX_DDL.DROP_INDEX_SET('movie_set');
end;

-- End Suppress Warnings

create INDEX CUSTOMER_CONTACT_EMAIL_INDEX
    on CUSTOMER_CONTACT (EMAIL) &logging;

create INDEX CUSTOMER_CONTACT_FIRST_NAME_INDEX
    on CUSTOMER_CONTACT (FIRST_NAME) &logging;

create INDEX CUSTOMER_CONTACT_LAST_NAME_INDEX
    on CUSTOMER_CONTACT (LAST_NAME) &logging;

create INDEX CUSTOMER_PAYMENT_INFORMATION_CUST_ID_INDEX
    on CUSTOMER_PAYMENT_INFORMATION (CUST_ID) &logging;

create INDEX CUSTOMER_DEVICE_CUST_ID_INDEX
    on CUSTOMER_DEVICE (CUST_ID) &logging;

create INDEX CUSTOMER_FRIENDS_AND_FAMILY_CUSTOMER_SOURCE_INDEX
    on CUSTOMER_FRIENDS_AND_FAMILY (CUSTOMER_SOURCE) &logging;

create INDEX CUSTOMER_FRIENDS_AND_FAMILY_CUSTOMER_TARGET_INDEX
    on CUSTOMER_FRIENDS_AND_FAMILY (CUSTOMER_TARGET) &logging;

create INDEX CUSTOMER_REVIEW_CUST_ID_INDEX
    on CUSTOMER_REVIEW (CUST_ID) &logging;

create INDEX CUSTOMER_REVIEW_MOVIE_ID_INDEX
    on CUSTOMER_REVIEW (MOVIE_ID) &logging;

create INDEX CUSTOMER_SALES_TRANSACTION_CUSTOMER_PAYMENT_INFORMATION_ID_INDEX
    on CUSTOMER_SALES_TRANSACTION (CUSTOMER_PAYMENT_INFORMATION_ID) &logging;

create INDEX CUSTOMER_SALES_TRANSACTION_MOVIE_ID_INDEX
    on CUSTOMER_SALES_TRANSACTION (MOVIE_ID) &logging;

CREATE INDEX movie_genres_index ON movie (GENRES)
    INDEXTYPE IS CTXSYS.CONTEXT
    parameters('section group ctxsys.JSON_SECTION_GROUP SYNC (ON COMMIT)');

CREATE INDEX movie_cast_index ON movie (CAST)
    INDEXTYPE IS CTXSYS.CONTEXT
    parameters('section group ctxsys.JSON_SECTION_GROUP SYNC (ON COMMIT)');

begin
    ctx_ddl.create_index_set('movie_set');
    ctx_ddl.add_index('movie_set', 'year');
    ctx_ddl.add_index('movie_set', 'views');
    ctx_ddl.add_index('movie_set', 'gross');
end;

CREATE INDEX movie_title_index ON movie (title)
    INDEXTYPE IS CTXSYS.CTXCAT;

CREATE INDEX movie_summary_index ON movie (summary)
    INDEXTYPE IS CTXSYS.CTXCAT
    PARAMETERS ('INDEX SET movie_set');


