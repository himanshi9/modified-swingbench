DROP TABLE        customers             CASCADE CONSTRAINTS purge;

DROP TABLE        inventories           CASCADE CONSTRAINTS purge;

DROP TABLE        order_items           CASCADE CONSTRAINTS purge;

DROP TABLE        orders                CASCADE CONSTRAINTS purge;

DROP TABLE        addresses             CASCADE CONSTRAINTS purge;

DROP TABLE        addresses1             CASCADE CONSTRAINTS purge;

DROP TABLE        addresses2             CASCADE CONSTRAINTS purge;

DROP TABLE        addresses3             CASCADE CONSTRAINTS purge;

DROP TABLE        addresses4             CASCADE CONSTRAINTS purge;

DROP TABLE        addresses5             CASCADE CONSTRAINTS purge;

DROP TABLE        addresses6             CASCADE CONSTRAINTS purge;

DROP TABLE        addresses7             CASCADE CONSTRAINTS purge;

DROP TABLE        addresses8             CASCADE CONSTRAINTS purge;

DROP TABLE        addresses9             CASCADE CONSTRAINTS purge;

DROP TABLE        addresses10             CASCADE CONSTRAINTS purge;


DROP TABLE        card_details          CASCADE CONSTRAINTS purge;

DROP TABLE        product_descriptions  CASCADE CONSTRAINTS purge;

DROP TABLE        product_information   CASCADE CONSTRAINTS purge;

DROP TABLE        warehouses            CASCADE CONSTRAINTS purge;

DROP TABLE 		  logon                 CASCADE CONSTRAINTS purge;

DROP TABLE 		  orderentry_metadata   CASCADE CONSTRAINTS purge;

DROP TABLE 		  customers1   CASCADE CONSTRAINTS purge;

DROP TABLE 		  customers2   CASCADE CONSTRAINTS purge;

DROP TABLE 		  customers3   CASCADE CONSTRAINTS purge;

DROP TABLE 		  customers4   CASCADE CONSTRAINTS purge;

DROP TABLE 		  customers5   CASCADE CONSTRAINTS purge;

DROP TABLE 		  customers6   CASCADE CONSTRAINTS purge;

DROP TABLE 		  customers7   CASCADE CONSTRAINTS purge;

DROP TABLE 		  customers8   CASCADE CONSTRAINTS purge;

DROP TABLE 		  customers9   CASCADE CONSTRAINTS purge;

DROP TABLE 		  customers10   CASCADE CONSTRAINTS purge;

DROP TABLE 		  customers11  CASCADE CONSTRAINTS purge;

DROP TABLE 		  customers12   CASCADE CONSTRAINTS purge;

DROP TABLE 		  customers13   CASCADE CONSTRAINTS purge;

DROP TABLE 		  customers14   CASCADE CONSTRAINTS purge;

DROP TABLE 		  customers15   CASCADE CONSTRAINTS purge;

DROP SEQUENCE     orders_seq;

DROP SEQUENCE     customer_seq;

DROP SEQUENCE	  card_details_seq;

DROP SEQUENCE     address_seq;

DROP SEQUENCE     address_seq1;

DROP SEQUENCE     lineitem_seq;

DROP VIEW         product_prices;

DROP VIEW         products;

COMMIT;


