CREATE UNIQUE INDEX customers_pk ON customers (customer_id) reverse tablespace &indextablespace &parallelclause &logging;

ALTER TABLE customers
ADD ( CONSTRAINT customers_pk PRIMARY KEY (customer_id) novalidate);

ALTER TABLE CUSTOMERS
ADD ( CONSTRAINT customer_credit_limit_max CHECK (credit_limit <= 50000) DEFERRABLE NOVALIDATE );

ALTER TABLE CUSTOMERS
ADD ( CONSTRAINT customer_id_min CHECK (customer_id > 0) DEFERRABLE NOVALIDATE );

CREATE UNIQUE INDEX address_pk ON addresses (address_id) reverse tablespace &indextablespace &parallelclause &logging;

ALTER TABLE addresses
ADD ( CONSTRAINT address_pk PRIMARY KEY (address_id) novalidate);

ALTER TABLE addresses
ADD (CONSTRAINT add_cust_fk FOREIGN KEY (CUSTOMER_ID) REFERENCES CUSTOMERS(CUSTOMER_ID) DEFERRABLE NOVALIDATE);


ALTER TABLE customers1
ADD ( CONSTRAINT customers_pk1 PRIMARY KEY (customer_id) novalidate);

ALTER TABLE addresses1
ADD ( CONSTRAINT address_pk1 PRIMARY KEY (address_id) novalidate);
--
--ALTER TABLE addresses1
--ADD (CONSTRAINT add_cust_fk1 FOREIGN KEY (CUSTOMER_ID) REFERENCES CUSTOMERS1(CUSTOMER_ID) DEFERRABLE NOVALIDATE);

CREATE UNIQUE INDEX address_pk1 ON addresses1 (address_id) reverse tablespace &indextablespace &parallelclause &logging;


ALTER TABLE customers2
ADD ( CONSTRAINT customers_pk2 PRIMARY KEY (customer_id) novalidate);

ALTER TABLE addresses2
ADD ( CONSTRAINT address_pk2 PRIMARY KEY (address_id) novalidate);

--ALTER TABLE addresses2
--ADD (CONSTRAINT add_cust_fk2 FOREIGN KEY (CUSTOMER_ID) REFERENCES CUSTOMERS2(CUSTOMER_ID) DEFERRABLE NOVALIDATE);

CREATE UNIQUE INDEX address_pk2 ON addresses2 (address_id) reverse tablespace &indextablespace &parallelclause &logging;


ALTER TABLE customers3
ADD ( CONSTRAINT customers_pk3 PRIMARY KEY (customer_id) novalidate);

ALTER TABLE addresses3
ADD ( CONSTRAINT address_pk3 PRIMARY KEY (address_id) novalidate);

--ALTER TABLE addresses3
--ADD (CONSTRAINT add_cust_fk3 FOREIGN KEY (CUSTOMER_ID) REFERENCES CUSTOMERS3(CUSTOMER_ID) DEFERRABLE NOVALIDATE);

CREATE UNIQUE INDEX address_pk3 ON addresses3 (address_id) reverse tablespace &indextablespace &parallelclause &logging;

ALTER TABLE customers4
ADD ( CONSTRAINT customers_pk4 PRIMARY KEY (customer_id) novalidate);

ALTER TABLE addresses4
ADD ( CONSTRAINT address_pk4 PRIMARY KEY (address_id) novalidate);

--ALTER TABLE addresses4
--ADD (CONSTRAINT add_cust_fk4 FOREIGN KEY (CUSTOMER_ID) REFERENCES CUSTOMERS4(CUSTOMER_ID) DEFERRABLE NOVALIDATE);

CREATE UNIQUE INDEX address_pk4 ON addresses4 (address_id) reverse tablespace &indextablespace &parallelclause &logging;


ALTER TABLE customers5
ADD ( CONSTRAINT customers_pk5 PRIMARY KEY (customer_id) novalidate);

ALTER TABLE addresses5
ADD ( CONSTRAINT address_pk5 PRIMARY KEY (address_id) novalidate);

--ALTER TABLE addresses5
--ADD (CONSTRAINT add_cust_fk5 FOREIGN KEY (CUSTOMER_ID) REFERENCES CUSTOMERS5(CUSTOMER_ID) DEFERRABLE NOVALIDATE);

CREATE UNIQUE INDEX address_pk5 ON addresses5 (address_id) reverse tablespace &indextablespace &parallelclause &logging;


ALTER TABLE customers6
ADD ( CONSTRAINT customers_pk6 PRIMARY KEY (customer_id) novalidate);

ALTER TABLE addresses6
ADD ( CONSTRAINT address_pk6 PRIMARY KEY (address_id) novalidate);

--ALTER TABLE addresses6
--ADD (CONSTRAINT add_cust_fk6 FOREIGN KEY (CUSTOMER_ID) REFERENCES CUSTOMERS6(CUSTOMER_ID) DEFERRABLE NOVALIDATE);

CREATE UNIQUE INDEX address_pk6 ON addresses6 (address_id) reverse tablespace &indextablespace &parallelclause &logging;


ALTER TABLE customers7
ADD ( CONSTRAINT customers_pk7 PRIMARY KEY (customer_id) novalidate);

ALTER TABLE addresses7
ADD ( CONSTRAINT address_pk7 PRIMARY KEY (address_id) novalidate);

--ALTER TABLE addresses7
--ADD (CONSTRAINT add_cust_fk7 FOREIGN KEY (CUSTOMER_ID) REFERENCES CUSTOMERS7(CUSTOMER_ID) DEFERRABLE NOVALIDATE);

CREATE UNIQUE INDEX address_pk7 ON addresses7 (address_id) reverse tablespace &indextablespace &parallelclause &logging;


ALTER TABLE customers8
ADD ( CONSTRAINT customers_pk8 PRIMARY KEY (customer_id) novalidate);

ALTER TABLE addresses8
ADD ( CONSTRAINT address_pk8 PRIMARY KEY (address_id) novalidate);

--ALTER TABLE addresses8
--ADD (CONSTRAINT add_cust_fk8 FOREIGN KEY (CUSTOMER_ID) REFERENCES CUSTOMERS8(CUSTOMER_ID) DEFERRABLE NOVALIDATE);

CREATE UNIQUE INDEX address_pk8 ON addresses8 (address_id) reverse tablespace &indextablespace &parallelclause &logging;


ALTER TABLE customers9
ADD ( CONSTRAINT customers_pk9 PRIMARY KEY (customer_id) novalidate);

ALTER TABLE addresses9
ADD ( CONSTRAINT address_pk9 PRIMARY KEY (address_id) novalidate);

--ALTER TABLE addresses9
--ADD (CONSTRAINT add_cust_fk9 FOREIGN KEY (CUSTOMER_ID) REFERENCES CUSTOMERS9(CUSTOMER_ID) DEFERRABLE NOVALIDATE);

CREATE UNIQUE INDEX address_pk9 ON addresses9 (address_id) reverse tablespace &indextablespace &parallelclause &logging;


ALTER TABLE customers10
ADD ( CONSTRAINT customers_pk10 PRIMARY KEY (customer_id) novalidate);

ALTER TABLE addresses10
ADD ( CONSTRAINT address_pk10 PRIMARY KEY (address_id) novalidate);

--ALTER TABLE addresses10
--ADD (CONSTRAINT add_cust_fk10 FOREIGN KEY (CUSTOMER_ID) REFERENCES CUSTOMERS10(CUSTOMER_ID) DEFERRABLE NOVALIDATE);

CREATE UNIQUE INDEX address_pk10 ON addresses10 (address_id) reverse tablespace &indextablespace &parallelclause &logging;


CREATE UNIQUE INDEX card_details_pk ON card_details (card_id) reverse tablespace &indextablespace &parallelclause &logging;

ALTER TABLE card_details
ADD ( CONSTRAINT card_details_pk PRIMARY KEY (card_id) novalidate);

CREATE UNIQUE INDEX warehouses_pk ON warehouses (warehouse_id) tablespace &indextablespace &parallelclause &logging;

ALTER TABLE warehouses
ADD (CONSTRAINT warehouses_pk PRIMARY KEY (warehouse_id) novalidate);

CREATE UNIQUE INDEX order_items_pk ON order_items (order_id, line_item_id) reverse tablespace &indextablespace &parallelclause &logging;

-- CREATE UNIQUE INDEX order_items_uk ON order_items (order_id, product_id) tablespace &indextablespace nologging;

ALTER TABLE order_items
ADD ( CONSTRAINT order_items_pk PRIMARY KEY (order_id, line_item_id) novalidate);

--ALTER TABLE order_items1
--ADD ( CONSTRAINT order_items_pk1 PRIMARY KEY (order_id, line_item_id) novalidate);

CREATE UNIQUE INDEX order_pk ON orders (order_id) reverse tablespace &indextablespace &parallelclause &logging;

--CREATE UNIQUE INDEX order_pk1 ON orders1 (order_id) reverse tablespace &indextablespace &parallelclause &logging;

ALTER TABLE orders 
ADD ( CONSTRAINT order_pk PRIMARY KEY (order_id) novalidate);

--ALTER TABLE orders1 
--ADD ( CONSTRAINT order_pk1 PRIMARY KEY (order_id) novalidate);

ALTER TABLE product_information
ADD ( CONSTRAINT product_information_pk PRIMARY KEY (product_id));

CREATE UNIQUE INDEX prd_desc_pk ON product_descriptions(product_id,language_id) tablespace &indextablespace &parallelclause &logging;

ALTER TABLE ORDERS
ADD (constraint order_mode_lov CHECK(order_mode in ('direct','online')) DEFERRABLE NOVALIDATE );

ALTER TABLE ORDERS
ADD (constraint order_total_min CHECK(order_total >= 0) DEFERRABLE NOVALIDATE );

ALTER TABLE product_descriptions
ADD ( CONSTRAINT product_descriptions_pk PRIMARY KEY (product_id, language_id) novalidate);

ALTER TABLE orders ADD ( CONSTRAINT orders_customer_id_fk FOREIGN KEY (customer_id) REFERENCES customers(customer_id) ON DELETE SET NULL novalidate) ;

--ALTER TABLE orders1 ADD ( CONSTRAINT orders_customer_id_fk1 FOREIGN KEY (customer_id) REFERENCES customers(customer_id) ON DELETE SET NULL novalidate) ;

ALTER TABLE inventories
ADD (CONSTRAINT inventory_pk PRIMARY KEY (product_id, warehouse_id) novalidate);

ALTER TABLE inventories
ADD ( CONSTRAINT inventories_warehouses_fk FOREIGN KEY (warehouse_id) REFERENCES warehouses (warehouse_id) ENABLE NOVALIDATE ) ;

ALTER TABLE inventories
ADD ( CONSTRAINT inventories_product_id_fk FOREIGN KEY (product_id) REFERENCES product_information (product_id) novalidate);

ALTER TABLE order_items
ADD ( CONSTRAINT order_items_order_id_fk FOREIGN KEY (order_id) REFERENCES orders(order_id) ON DELETE CASCADE enable novalidate );

--ALTER TABLE order_items1
--ADD ( CONSTRAINT order_items_order_id_fk1 FOREIGN KEY (order_id) REFERENCES orders(order_id) ON DELETE CASCADE enable novalidate );

ALTER TABLE order_items
ADD ( CONSTRAINT order_items_product_id_fk FOREIGN KEY (product_id) REFERENCES product_information(product_id) novalidate);

--ALTER TABLE order_items1
--ADD ( CONSTRAINT order_items_product_id_fk1 FOREIGN KEY (product_id) REFERENCES product_information(product_id) novalidate);

ALTER TABLE product_descriptions
ADD ( CONSTRAINT pd_product_id_fk FOREIGN KEY (product_id) REFERENCES product_information(product_id));
