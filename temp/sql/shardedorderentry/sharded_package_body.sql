create or replace PACKAGE BODY ORDERENTRY
AS
  MAX_BROWSE_CATEGORY INTEGER:= 6;
  MIN_PROD_ID         INTEGER:= 1;
  MAX_PROD_ID         INTEGER:= 1000;
  SELECT_STATEMENTS   INTEGER:= 1;
  INSERT_STATEMENTS   INTEGER:= 2;
  UPDATE_STATEMENTS   INTEGER:= 3;
  DELETE_STATEMENTS   INTEGER:= 4;
  COMMIT_STATEMENTS   INTEGER:= 5;
  ROLLBACK_STATEMENTS INTEGER:= 6;
  SLEEP_TIME          INTEGER:= 7;
  MIN_CATEGORY        INTEGER:= 1;
  MAX_CATEGORY        INTEGER:= 199;
  MIN_PRODS_TO_BUY    INTEGER:= 2;
  MAX_PRODS_TO_BUY    INTEGER:= 6;
  MIN_QUANTITY        INTEGER:= 2;
  MAX_QUANTITY        INTEGER:= 6;
  AWAITING_PROCESSING INTEGER:= 4;
  ORDER_PROCESSED     INTEGER:= 10;
  MAX_CREDITLIMIT     INTEGER:= 5000;
  MIN_CREDITLIMIT     INTEGER:= 100;
  MIN_SALESID         INTEGER:= 145;
  MAX_SALESID         INTEGER:= 171;
  MIN_COST_DELIVERY   INTEGER:= 1;
  MAX_COST_DELIVERY   INTEGER:= 5;
  ROW_RETURNS         INTEGER:= 15;
  HOUSE_NO_RANGE      INTEGER:= 200;
  PLSQLCOMMIT         BOOLEAN:= false;
  CUSTOMER_CLASS customers.customer_class%type     := 'Occasional';
  SUGGESTIONS customers.suggestions%type           := 'Books';
  MAILSHOT customers.mailshot%type                 := 'Y';
  PARTNER_MAILSHOT customers.partner_mailshot%type := 'Y';
  INST_ID INTEGER            := sys_context('userenv','instance');
  INFO_ARRAY integer_return_array                  := integer_return_array();
  PROCEDURE setPLSQLCOMMIT(
      commitInPLSQL VARCHAR2)
  AS
  BEGIN
    PLSQLCOMMIT := false;
  END setPLSQLCOMMIT;
  PROCEDURE oecommit
  IS
  BEGIN
    IF (PLSQLCOMMIT) THEN
      COMMIT;
    END IF;
  END oecommit;
  FUNCTION from_mills_to_secs(
      value INTEGER)
    RETURN FLOAT
  IS
    real_value FLOAT := 0;
  BEGIN
    real_value := value/1000;
    RETURN real_value;
  EXCEPTION
  WHEN zero_divide THEN
    real_value := 0;
    RETURN real_value;
  END from_mills_to_secs;
  PROCEDURE sleep(
      min_sleep INTEGER,
      max_sleep INTEGER)
  IS
    sleeptime  NUMBER := 0;
    beginSleep TIMESTAMP;
    endSleep   TIMESTAMP;
    myseconds  NUMBER :=0;
    myminutes  NUMBER := 0;
  BEGIN
    beginSleep     := systimestamp;
    IF (min_sleep  != 0) AND (max_sleep != 0) THEN
      IF (max_sleep = min_sleep) THEN
        sleeptime  := from_mills_to_secs(max_sleep);
        dbms_lock.sleep(sleeptime);
      elsif (((max_sleep - min_sleep) > 0) AND (min_sleep < max_sleep)) THEN
        sleeptime                    := dbms_random.value(from_mills_to_secs(min_sleep), from_mills_to_secs(max_sleep));
        dbms_lock.sleep(sleeptime);
      END IF;
      endSleep  := systimestamp;
      myseconds := extract(second FROM (endSleep-beginSleep));
      myminutes := extract(minute FROM (endSleep-beginSleep));
      sleepTime := (myseconds                   + (myminutes*60))*1000;
    ELSE
      sleepTime := 0;
    END IF;
    info_array(SLEEP_TIME) := sleeptime + info_array(SLEEP_TIME);
  END sleep;
  PROCEDURE init_info_array
  IS
  BEGIN
    info_array := integer_return_array();
    FOR i IN 1..8
    LOOP
      info_array.extend;
      info_array(i) := 0;
    END LOOP;
  END init_info_array;
  FUNCTION getdmlarrayasstring(
      info_array integer_return_array)
    RETURN VARCHAR
  IS
    result VARCHAR(200) := '';
  BEGIN
    result := info_array(SELECT_STATEMENTS) ||','|| info_array(INSERT_STATEMENTS)||','||info_array(UPDATE_STATEMENTS)||','||info_array(DELETE_STATEMENTS)||','||info_array(COMMIT_STATEMENTS)||','||info_array(ROLLBACK_STATEMENTS)||','||info_array(SLEEP_TIME);
    RETURN result;
  END getdmlarrayasstring;
  PROCEDURE increment_selects(
      num_selects INTEGER)
  IS
  BEGIN
    info_array(SELECT_STATEMENTS) := info_array(SELECT_STATEMENTS) + num_selects;
  END increment_selects;
  PROCEDURE increment_inserts(
      num_inserts INTEGER)
  IS
  BEGIN
    info_array(INSERT_STATEMENTS) := info_array(INSERT_STATEMENTS) + num_inserts;
  END increment_inserts;
  PROCEDURE increment_updates(
      num_updates INTEGER)
  IS
  BEGIN
    info_array(UPDATE_STATEMENTS) := info_array(UPDATE_STATEMENTS) + num_updates;
  END increment_updates;
  PROCEDURE increment_deletes(
      num_deletes INTEGER)
  IS
  BEGIN
    info_array(DELETE_STATEMENTS) := info_array(DELETE_STATEMENTS) + num_deletes;
  END increment_deletes;
  PROCEDURE increment_commits(
      num_commits INTEGER)
  IS
  BEGIN
    info_array(COMMIT_STATEMENTS) := info_array(COMMIT_STATEMENTS) + num_commits;
  END increment_commits;
  PROCEDURE increment_rollbacks(
      num_rollbacks INTEGER)
  IS
  BEGIN
    info_array(ROLLBACK_STATEMENTS) := info_array(ROLLBACK_STATEMENTS) + num_rollbacks;
  END increment_rollbacks;
  FUNCTION getProductDetailsByCategory(
      cat_id IN OUT product_information.category_id%type)
    RETURN prod_tab
  IS
    my_product_tab prod_tab;
  BEGIN
    dbms_application_info.set_action('getProductDetailsByCategory');
    SELECT products.PRODUCT_ID, PRODUCT_NAME, PRODUCT_DESCRIPTION, CATEGORY_ID, WEIGHT_CLASS, WARRANTY_PERIOD, SUPPLIER_ID, PRODUCT_STATUS, LIST_PRICE, MIN_PRICE, CATALOG_URL
    bulk collect
    INTO my_product_tab
    FROM products
    WHERE rownum < ROW_RETURNS;
    dbms_application_info.set_action(NULL);
    RETURN my_product_tab;
  END getProductDetailsByCategory;
  FUNCTION getCustomerDetails(
      cust_id customers.customer_id%type)
    RETURN customer_tab
  IS
    my_customer_tab customer_tab;
  BEGIN
    dbms_application_info.set_action('getCustomerDetails');
    SELECT CUSTOMER_ID, CUST_FIRST_NAME, CUST_LAST_NAME, NLS_LANGUAGE, NLS_TERRITORY, CREDIT_LIMIT, CUST_EMAIL, ACCOUNT_MGR_ID, CUSTOMER_SINCE, CUSTOMER_CLASS, SUGGESTIONS, DOB, MAILSHOT, PARTNER_MAILSHOT, PREFERRED_ADDRESS, PREFERRED_CARD bulk collect
    INTO my_customer_tab
    FROM CUSTOMERS
    WHERE customer_id = cust_id
    AND rownum        < ROW_RETURNS;
    dbms_application_info.set_action(NULL);
    RETURN my_customer_tab;
  END getCustomerDetails;
  FUNCTION getAddressDetails(
      cust_id addresses.customer_id%type)
    RETURN address_tab
  IS
    my_address_tab address_tab;
  BEGIN
    dbms_application_info.set_action('getAddressDetails');
    SELECT ADDRESS_ID, CUSTOMER_ID, DATE_CREATED, HOUSE_NO_OR_NAME, STREET_NAME, TOWN, COUNTY, COUNTRY, POST_CODE, ZIP_CODE bulk collect
    INTO my_address_tab
    FROM ADDRESSES
    WHERE customer_id = cust_id
    AND rownum        < ROW_RETURNS;
    dbms_application_info.set_action(NULL);
    RETURN my_address_tab;
  END getAddressDetails;
  FUNCTION getCardDetailsByCustomerID(
      cust_id addresses.customer_id%type)
    RETURN card_details_tab
  IS
    my_card_tab card_details_tab;
  BEGIN
    dbms_application_info.set_action('getCardDetailsByCustomerID');
    SELECT CARD_ID, CUSTOMER_ID, CARD_TYPE, CARD_NUMBER, EXPIRY_DATE, IS_VALID, SECURITY_CODE bulk collect
    INTO my_card_tab
    FROM card_details
    WHERE CUSTOMER_ID = cust_id
    AND rownum        < ROW_RETURNS;
    dbms_application_info.set_action(NULL);
    RETURN my_card_tab;
  END getCardDetailsByCustomerID;
  FUNCTION getProductDetails(
      prod_id product_information.product_id%type)
    RETURN prod_tab
  IS
    my_product_tab prod_tab;
  BEGIN
    dbms_application_info.set_action('getProductDetails');
    SELECT products.PRODUCT_ID, PRODUCT_NAME, PRODUCT_DESCRIPTION, CATEGORY_ID, WEIGHT_CLASS, WARRANTY_PERIOD, SUPPLIER_ID, PRODUCT_STATUS, LIST_PRICE, MIN_PRICE, CATALOG_URL bulk collect
    INTO my_product_tab
    FROM products
    WHERE rownum < ROW_RETURNS;
    dbms_application_info.set_action(NULL);
    RETURN my_product_tab;
  END getProductDetails;
  FUNCTION getOrdersById(
      ord_id orders.order_id%type)
    RETURN order_tab
  IS
    my_orders_tab order_tab;
  BEGIN
    dbms_application_info.set_action('getOrdersById');
    SELECT ORDER_ID, ORDER_DATE, ORDER_MODE, CUSTOMER_ID, ORDER_STATUS, ORDER_TOTAL, SALES_REP_ID, PROMOTION_ID, WAREHOUSE_ID, DELIVERY_TYPE, COST_OF_DELIVERY, WAIT_TILL_ALL_AVAILABLE, DELIVERY_ADDRESS_ID, CUSTOMER_CLASS, CARD_ID, INVOICE_ADDRESS_ID bulk collect
    INTO my_orders_tab
    FROM orders
    WHERE order_id = ord_id
    AND rownum     < ROW_RETURNS;
    dbms_application_info.set_action(NULL);
    RETURN my_orders_tab;
  END getOrdersById;
  FUNCTION getOrdersByCustomer(
      cust_id customers.customer_id%type)
    RETURN order_tab
  IS
    my_orders_tab order_tab;
  BEGIN
    dbms_application_info.set_action('getOrdersByCustomer');
    SELECT ORDER_ID, ORDER_DATE, ORDER_MODE, CUSTOMER_ID, ORDER_STATUS, ORDER_TOTAL, SALES_REP_ID, PROMOTION_ID, WAREHOUSE_ID, DELIVERY_TYPE, COST_OF_DELIVERY, WAIT_TILL_ALL_AVAILABLE, DELIVERY_ADDRESS_ID, CUSTOMER_CLASS, CARD_ID, INVOICE_ADDRESS_ID bulk collect
    INTO my_orders_tab
    FROM orders
    WHERE customer_id = cust_id
    AND rownum        < ROW_RETURNS;
    dbms_application_info.set_action(NULL);
    RETURN my_orders_tab;
  END getOrdersByCustomer;
  FUNCTION getOrderItemsById(
      ord_id orders.order_id%type)
    RETURN order_item_tab
  IS
    my_order_Item_tab order_item_tab;
  BEGIN
    dbms_application_info.set_action('getOrderItemsById');
    SELECT ORDER_ID, LINE_ITEM_ID, PRODUCT_ID, UNIT_PRICE, QUANTITY, DISPATCH_DATE, RETURN_DATE, GIFT_WRAP, CONDITION, SUPPLIER_ID, ESTIMATED_DELIVERY bulk collect
    INTO my_order_Item_tab
    FROM order_items
    WHERE order_id = ord_id
    AND rownum     < ROW_RETURNS;
    dbms_application_info.set_action(NULL);
    RETURN my_order_Item_tab;
  END getOrderItemsById;
  FUNCTION findCustomerByName(
      p_cust_first_name customers.cust_first_name%type,
      p_cust_last_name customers.cust_last_name%type)
    RETURN customer_tab
  IS
    my_customer_tab customer_tab;
  BEGIN
    dbms_application_info.set_action('findCustomer');
    SELECT CUSTOMER_ID, CUST_FIRST_NAME, CUST_LAST_NAME, NLS_LANGUAGE, NLS_TERRITORY, CREDIT_LIMIT, CUST_EMAIL, ACCOUNT_MGR_ID, CUSTOMER_SINCE, CUSTOMER_CLASS, SUGGESTIONS, DOB, MAILSHOT, PARTNER_MAILSHOT, PREFERRED_ADDRESS, PREFERRED_CARD bulk collect
    INTO my_customer_tab
    FROM CUSTOMERS
    WHERE lower(cust_last_name) = lower(p_cust_last_name)
    AND lower(cust_first_name)  = lower(p_cust_first_name)
    AND rownum                  < ROW_RETURNS;
    dbms_application_info.set_action(NULL);
    RETURN my_customer_tab;
  EXCEPTION
  WHEN NO_DATA_FOUND THEN
    RETURN my_customer_tab;
  END findCustomerByName;
  PROCEDURE logon(
      cust_id customers.customer_id%type)
  IS
    CURRENT_TIME DATE;
  BEGIN
    dbms_application_info.set_action('logon');
    SELECT sysdate INTO CURRENT_TIME FROM dual;
    INSERT INTO logon ( logon_id, customer_id, logon_date ) VALUES ( logon_seq.nextval, cust_id, CURRENT_TIME );
    oecommit;
    dbms_application_info.set_action(NULL);
  END logon;
  FUNCTION browseProducts
    (
      cust_id customers.customer_id%type,
      min_sleep INTEGER,
      max_sleep INTEGER
    )
    RETURN VARCHAR
  IS
    customerArray customer_tab;
    productsArray prod_tab;
    prodId       INTEGER;
    numOfBrowses INTEGER := 1;
  BEGIN
    dbms_application_info.set_module('Browse Products',NULL);
    init_info_array();
    customerArray := getCustomerDetails(cust_id);
    increment_selects(1);
    sleep(min_sleep, max_sleep);
    logon(cust_id);
    increment_inserts(1);
    increment_selects(1);
    increment_commits(1);
    sleep(min_sleep, max_sleep);
    numOfBrowses := floor(dbms_random.value(1,MAX_BROWSE_CATEGORY));
    FOR i IN 1..numOfBrowses
    LOOP
      prodId := floor
      (dbms_random.value(MIN_PROD_ID, MAX_PROD_ID));
      productsArray := getProductDetails(prodId);
      increment_selects(1);
      sleep(min_sleep, max_sleep);
    END LOOP;
    dbms_application_info.set_module(NULL,NULL);
    RETURN getdmlarrayasstring(info_array);
  EXCEPTION
  WHEN OTHERS THEN
    ROLLBACK;
    increment_rollbacks(1);
    dbms_application_info.set_module(NULL,NULL);
    RETURN getdmlarrayasstring(info_array);
  END browseProducts;
  FUNCTION newOrder
    (
      p_fname customers.cust_first_name%type,
      p_lname customers.cust_last_name%type,
      min_sleep INTEGER,
      max_sleep INTEGER
    )
    RETURN VARCHAR
  IS
    customerArray customer_tab;
    productsArray prod_tab;
    addressesArray address_tab;
    cardArray card_details_tab;
    numOfBrowses INTEGER := 1;
    catId        INTEGER := 1;
    prodId       INTEGER := 1;
    prodsToBuy   INTEGER := 1;
    cust_id customers.customer_id%type;
    orderId orders.order_id%type;
    cardId card_details.card_id%type;
    addressId addresses.address_id%type;
    quantity INTEGER := 1;
    price FLOAT      := 1.0;
    totalPrice FLOAT := 0.0;
    l_cost NUMBER;
  type prodListType
IS
  TABLE OF PRODUCTS.PRODUCT_ID%type;
  prodList prodListType := prodListType();
BEGIN
  dbms_application_info.set_module('New Order',NULL);
  init_info_array();
  customerArray           := findCustomerByName(p_fname, p_lname);
  IF customerArray.count() > 0 THEN
    increment_selects(1);
    cust_id        := customerArray(1).CUSTOMER_ID;
    addressesArray := getAddressDetails(cust_id);
    increment_selects(1);
    cardArray := getcarddetailsbycustomerid(cust_id);
    increment_selects(1);
    sleep(min_sleep, max_sleep);
    logon(cust_id);
    increment_inserts(1);
    increment_selects(1);
    increment_commits(1);
    sleep(min_sleep, max_sleep);
    numOfBrowses := floor(dbms_random.value(1,MAX_BROWSE_CATEGORY));
    FOR i IN 1..numOfBrowses
    LOOP
      catId := floor(dbms_random.value(MIN_CATEGORY, MAX_CATEGORY));
      productsArray := getProductDetailsByCategory(catId);
      increment_selects(1);
      sleep(min_sleep, max_sleep);
    END LOOP;
    sleep(min_sleep, max_sleep);
    l_cost := floor(dbms_random.value(MIN_COST_DELIVERY, MAX_COST_DELIVERY));
    INSERT INTO orders ( order_id, order_date, order_mode, customer_id, order_status, warehouse_id, delivery_type, delivery_address_id, cost_of_delivery, wait_till_all_available, customer_class, card_id, invoice_address_id ) VALUES ( orders_seq.NEXTVAL, systimestamp , 'online', cust_id, 1, 1, 'Standard', customerArray(1).preferred_address, l_cost, 'ship_asap', customerArray(1).customer_class, customerArray(1).preferred_card, customerArray(1).preferred_address )
    RETURNING order_id
    INTO orderId;
    increment_inserts(1);
    sleep(min_sleep, max_sleep);
    prodsToBuy    := floor(dbms_random.value(MIN_PRODS_TO_BUY, MAX_PRODS_TO_BUY));
    IF (prodsToBuy > productsArray.count) THEN
      prodsToBuy  := productsArray.count;
    END IF;
    FOR i IN 1..prodsToBuy
    LOOP
      prodId := productsArray(i).product_id;
      price := productsArray(i).list_price;
      sleep(min_sleep, max_sleep);
      quantity   := floor(dbms_random.value(MIN_QUANTITY, MAX_QUANTITY));
      IF quantity > 0 THEN
        INSERT
        INTO order_items ( order_id, line_item_id, product_id, customer_id, unit_price, quantity, gift_wrap, condition, estimated_delivery ) VALUES ( orderId, i, prodId, cust_id, price, 1, 'None', 'New', (sysdate + 3) );
        increment_inserts(1);
        prodList.extend;
        prodList(prodList.count) := prodID;
        totalPrice := totalPrice + price;
      END IF;
      sleep(min_sleep, max_sleep);
    END LOOP;
    UPDATE orders
    SET order_mode = 'online',
      order_status = floor(dbms_random.value(0, AWAITING_PROCESSING)),
      order_total  = totalPrice
    WHERE order_id = orderId;
    increment_updates(1);
    oecommit;
    increment_commits(1);
    dbms_application_info.set_module(NULL,NULL);
    RETURN getdmlarrayasstring(info_array);
  ELSE
    increment_selects(1);
    RETURN getdmlarrayasstring(info_array);
  END IF;
EXCEPTION
WHEN OTHERS THEN
  DBMS_OUTPUT.PUT_LINE('Error code ' || SQLCODE || ': ' || SUBSTR(SQLERRM, 1 , 120));
  ROLLBACK;
  increment_rollbacks(1);
  dbms_application_info.set_module(NULL,NULL);
  RETURN getdmlarrayasstring(info_array);
END newOrder;
FUNCTION getAddressesByCustomerID(
    custid addresses.customer_id%type)
  RETURN address_tab
IS
  my_address_tab address_tab;
BEGIN
  dbms_application_info.set_action('getAddressesByCustomerID');
  SELECT ADDRESS_ID, CUSTOMER_ID, HOUSE_NO_OR_NAME, STREET_NAME, TOWN, COUNTY, COUNTRY, POST_CODE, ZIP_CODE, DATE_CREATED bulk collect
  INTO my_address_tab
  FROM ADDRESSES
  WHERE CUSTOMER_ID = custid
  AND rownum        < ROW_RETURNS;
  dbms_application_info.set_action(NULL);
  RETURN my_address_tab;
END getAddressesByCustomerID;
FUNCTION newCustomer(
    p_custd_id customers.customer_id%type,
    p_fname customers.cust_first_name%type,
    p_lname customers.cust_last_name%type,
    p_nls_lang customers.nls_language%type,
    p_nls_terr customers.nls_territory%type,
    p_town addresses.town%type,
    p_county addresses.county%type,
    p_country addresses.country%type,
    p_min_sleep INTEGER,
    p_max_sleep INTEGER)
  RETURN VARCHAR
IS
  l_customer_id customers.customer_id%type;
  l_address_id addresses.address_id%type;
  l_card_id card_details.card_id%type;
  l_customerArray customer_tab;
  l_credit_limit CUSTOMERS.CREDIT_LIMIT%type;
  l_account_mgr_id CUSTOMERS.account_mgr_id%type;
  l_dob CUSTOMERS.dob%type;
BEGIN
  dbms_application_info.set_module('Update Customer Details',NULL);
  init_info_array();
  -- select   customer_seq.nextval into l_customer_id from dual;-- this could be returned more efficently using returns clause on insert
  l_customer_id    := p_custd_id;
  l_credit_limit   := FLOOR(DBMS_RANDOM.value(MIN_CREDITLIMIT, MAX_CREDITLIMIT));
  l_account_mgr_id := FLOOR(DBMS_RANDOM.value(MIN_SALESID, MAX_SALESID));
  l_dob            := TRUNC(SYSDATE - (365*DBMS_RANDOM.value(20, 60)));
  SELECT address_seq.nextval INTO l_address_id FROM dual;
  SELECT card_details_seq.nextval INTO l_card_id FROM dual;
  increment_selects(3);
  INSERT
  INTO customers ( customer_id , cust_first_name , cust_last_name , nls_language , nls_territory , credit_limit , cust_email , account_mgr_id, customer_since, customer_class, suggestions, dob, mailshot, partner_mailshot, preferred_address, preferred_card ) VALUES ( l_customer_id, p_fname, p_lname, p_nls_lang, p_nls_terr, l_credit_limit, p_fname ||'.' ||p_lname ||'@' ||'oracle.com', l_account_mgr_id, TRUNC(SYSDATE), 'Occasional', 'Music', l_dob, 'Y', 'N', l_address_id, l_card_id );
  increment_inserts(1);
  INSERT
  INTO ADDRESSES ( address_id, customer_id, date_created, house_no_or_name, street_name, town, county, country, post_code, zip_code ) VALUES ( l_address_Id, l_customer_Id, TRUNC(SYSDATE,'MI'), floor(DBMS_RANDOM.value(1, HOUSE_NO_RANGE)), 'Street Name', p_town, p_county, p_country, 'Postcode', NULL );
  increment_inserts(1);
  INSERT
  INTO CARD_DETAILS ( CARD_ID, CUSTOMER_ID, CARD_TYPE, CARD_NUMBER, EXPIRY_DATE, IS_VALID, SECURITY_CODE ) VALUES ( l_card_id, l_customer_Id, 'Visa(Debit)', floor(DBMS_RANDOM.value(1111111111, 9999999999)), TRUNC(SYSDATE + (DBMS_RANDOM.value(365, 1460))), 'Y', floor(DBMS_RANDOM.value(1111, 9999)) );
  oecommit;
  increment_inserts(1);
  increment_commits(1);
  sleep(p_min_sleep, p_max_sleep);
  logon(l_customer_id);
  increment_inserts(1);
  increment_selects(1);
  increment_commits(1);
  l_customerArray := getCustomerDetails(l_customer_id);
  increment_selects(1);
  dbms_application_info.set_module(NULL,NULL);
  RETURN getdmlarrayasstring(info_array);
END newcustomer;
FUNCTION updateCustomerDetails
  (
    p_fname customers.cust_first_name%type,
    p_lname customers.cust_last_name%type,
    p_town addresses.town%type,
    p_county addresses.county%type,
    p_country addresses.country%type,
    min_sleep INTEGER,
    max_sleep INTEGER
  )
  RETURN VARCHAR
IS
  l_customer_array customer_tab;
  l_customer_id customers.customer_id%type;
  l_address_id addresses.address_id%type;
BEGIN
  dbms_application_info.set_module('Update Customer Details',NULL);
  init_info_array();
  l_customer_array := findCustomerByName(p_fname, p_lname);
  increment_selects(1);
  sleep(min_sleep, max_sleep);
  IF l_customer_array.count > 1 THEN
    SELECT address_seq.nextval INTO l_address_id FROM dual;
    l_customer_id := l_customer_array(1).customer_id;
    increment_selects(1);
    INSERT
    INTO ADDRESSES ( address_id, customer_id, date_created, house_no_or_name, street_name, town, county, country, post_code, zip_code ) VALUES ( l_address_Id, l_customer_Id, TRUNC(SYSDATE,'MI'), floor(DBMS_RANDOM.value(1, HOUSE_NO_RANGE)), 'Street Name', p_town, p_county, p_country, 'Postcode', NULL );
    increment_inserts(1);
    UPDATE CUSTOMERS
    SET PREFERRED_ADDRESS = l_address_id
    WHERE customer_id     = l_customer_id;
    oecommit;
    increment_updates(1);
    increment_commits(1);
  END IF;
  RETURN getdmlarrayasstring(info_array);
EXCEPTION
WHEN OTHERS THEN
  DBMS_OUTPUT.PUT_LINE('Error code ' || SQLCODE || ': ' || SUBSTR(SQLERRM, 1 , 64));
  ROLLBACK;
  increment_rollbacks(1);
  dbms_application_info.set_module(NULL,NULL);
  RETURN getdmlarrayasstring(info_array);
END updateCustomerDetails;
FUNCTION browseAndUpdateOrders(
    p_fname customers.cust_first_name%type,
    p_lname customers.cust_last_name%type,
    min_sleep INTEGER,
    max_sleep INTEGER)
  RETURN VARCHAR
IS
  ordId orders.order_id%type;
  lineId order_items.line_item_id%type;
  unitPrice order_items.unit_price%type;
  selectedLine    INTEGER := 1;
  selectedOrder   INTEGER := 1;
  selectedAddress INTEGER := 1;
  customerArray customer_tab;
  addressArray address_tab;
  orderArray order_tab;
  orderItemsArray order_item_tab;
  cust_id customers.customer_id%type;
BEGIN
  dbms_application_info.set_module('Browse and Update Orders',NULL);
  init_info_array();
  customerArray           := findCustomerByName(p_fname, p_lname);
  IF customerArray.count() > 0 THEN
    cust_id        := customerArray(1).CUSTOMER_ID;
    addressArray          := getAddressDetails(cust_id);
    increment_selects(2);
    sleep(min_sleep, max_sleep);
    logon(cust_id);
    increment_inserts(1);
    increment_selects(1);
    increment_commits(1);
    sleep(min_sleep, max_sleep);
    orderArray := getOrdersByCustomer(cust_id);
    increment_selects(1);
    IF orderArray.count > 1 THEN
      selectedOrder    := floor(dbms_random.value(1, orderArray.count));
      ordId            := orderArray(selectedOrder).order_id;
      orderItemsArray  := getOrderItemsById(ordId);
      increment_selects(1);
      selectedLine := floor(dbms_random.value(1, orderItemsArray.count));
      lineId       := orderItemsArray(selectedLine).line_item_id;
      unitPrice    := orderItemsArray(selectedLine).unit_price;
      UPDATE order_items SET quantity = quantity + 1 WHERE order_items.ORDER_Id   = ordId AND order_items.LINE_ITEM_ID = lineId;
      UPDATE orders SET order_total = order_total + unitPrice WHERE order_Id  = ordId;
      increment_updates(2);
      oecommit;
      increment_commits(1);
      RETURN getdmlarrayasstring(info_array);
    ELSE
      RETURN getdmlarrayasstring(info_array);
    END IF;
  ELSE
    increment_selects(1);
    RETURN getdmlarrayasstring(info_array);
  END IF;
  dbms_application_info.set_module(NULL,NULL);
EXCEPTION
WHEN OTHERS THEN
  ROLLBACK;
  increment_rollbacks(1);
  dbms_application_info.set_module(NULL,NULL);
  RETURN getdmlarrayasstring(info_array);
END browseAndUpdateOrders;
FUNCTION SalesRepsQuery(
    salesRep orders.sales_rep_id%type,
    min_sleep INTEGER,
    max_sleep INTEGER)
  RETURN VARCHAR
IS
  CURSOR c1
  IS
    SELECT tt.ORDER_TOTAL, tt.SALES_REP_ID, tt.ORDER_DATE, customers.CUST_FIRST_NAME, customers.CUST_LAST_NAME
    FROM
      (SELECT orders.ORDER_TOTAL, orders.SALES_REP_ID, orders.ORDER_DATE, orders.customer_id, rank() Over (Order By orders.ORDER_TOTAL DESC) sal_rank
      FROM orders
      WHERE orders.SALES_REP_ID = salesRep
      ) tt,
    customers
  WHERE tt.sal_rank        <= 10
  AND customers.customer_id = tt.customer_id;
  order_total orders.order_total%type;
  sales_rep_id orders.sales_rep_id%type;
  order_date orders.order_date%type;
  cust_first_name customers.cust_first_name%type;
  cust_last_name customers.cust_last_name%type;
BEGIN
  dbms_application_info.set_module('Sales Rep Query',NULL);
  init_info_array();
  OPEN c1;
  LOOP
    FETCH c1
    INTO order_total,
      sales_rep_id,
      order_date,
      cust_first_name,
      cust_last_name;
    EXIT
  WHEN c1%notfound;
  END LOOP;
  increment_selects(1);
  sleep(min_sleep, max_sleep);
  dbms_application_info.set_module(NULL,NULL);
  RETURN getdmlarrayasstring(info_array);
EXCEPTION
WHEN OTHERS THEN
  dbms_application_info.set_module(NULL,NULL);
  RETURN getdmlarrayasstring(info_array);
END SalesRepsQuery;
END ORDERENTRY;
/