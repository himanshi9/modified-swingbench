
alter session enable sharded ddl;

drop index address_cust_ix;
drop index item_order_ix;
drop index item_product_ix;
drop index ord_sales_rep_ix;
drop index ord_order_date_ix;
drop index ord_order_id_ix;
drop index cust_account_manager_ix;
drop index cust_dob_ix;
drop index cust_email_ix;
drop index prod_name_ix;
drop index prod_supplier_ix;
drop index prod_category_ix;
drop index cust_func_lower_firstname_ix;
drop index cust_func_lower_lastname_ix;
drop index carddetails_cust_ix;

CREATE INDEX address_cust_ix
ON addresses (customer_id) local 
tablespace set soe_ts_set
REVERSE nologging;

CREATE INDEX item_order_ix
ON order_items (order_id) local 
tablespace set soe_ts_set
REVERSE nologging;

CREATE INDEX item_product_ix
ON order_items (product_id) local
tablespace set soe_ts_set
REVERSE nologging;

CREATE INDEX ord_sales_rep_ix
ON orders (sales_rep_id)  local
tablespace set soe_ts_set
REVERSE nologging;

CREATE INDEX ord_order_id_ix
ON orders (order_id)  local
tablespace set soe_ts_set
REVERSE nologging;

CREATE INDEX ord_order_date_ix
ON orders (order_date) local
tablespace set soe_ts_set
REVERSE nologging;

--CREATE INDEX ord_status_ix
--ON orders (order_status) nologging;

CREATE INDEX cust_account_manager_ix
ON customers (account_mgr_id) local
tablespace set soe_ts_set
nologging;

CREATE INDEX cust_dob_ix
ON customers (dob) local
tablespace set soe_ts_set
nologging;

CREATE INDEX cust_email_ix
ON customers (cust_email) local
tablespace set soe_ts_set
nologging;

CREATE INDEX cust_func_lower_firstname_ix
ON customers lower(cust_first_name) local
tablespace set soe_ts_set
nologging;

CREATE INDEX cust_func_lower_lastname_ix
ON customers lower(cust_last_name) local
tablespace set soe_ts_set
nologging;

CREATE INDEX carddetails_cust_ix
ON card_details(customer_id) local
tablespace set soe_ts_set
nologging;


CREATE INDEX prod_name_ix
ON product_descriptions (translated_name)
tablespace products
nologging;

CREATE INDEX prod_supplier_ix
ON product_information (supplier_id) 
tablespace products
nologging;

CREATE INDEX prod_category_ix
ON product_information (category_id) 
tablespace products
nologging;



--End
