ALTER SESSION ENABLE SHARD DDL;

drop table ADDRESSES cascade CONSTRAINTs purge;
drop table CARD_DETAILS cascade CONSTRAINTs purge;
drop table LOGON cascade CONSTRAINTs purge;
drop table ORDER_ITEMS cascade CONSTRAINTs purge;
drop table ORDERS cascade CONSTRAINTs purge;
drop table PRODUCT_DESCRIPTIONS cascade CONSTRAINTs purge;
drop table PRODUCT_INFORMATION cascade CONSTRAINTs purge;
drop table ORDERENTRY_METADATA cascade CONSTRAINTs purge;
drop table CUSTOMERS cascade CONSTRAINTs purge;

CREATE SHARDED TABLE CUSTOMERS
  (
    CUSTOMER_ID       VARCHAR2(40 BYTE) NOT NULL,
    CUST_FIRST_NAME   VARCHAR2(40 BYTE),
    CUST_LAST_NAME    VARCHAR2(40 BYTE),
    NLS_LANGUAGE      VARCHAR2(3 BYTE),
    NLS_TERRITORY     VARCHAR2(30 BYTE),
    CREDIT_LIMIT      NUMBER(9,2),
    CUST_EMAIL        VARCHAR2(100 BYTE),
    ACCOUNT_MGR_ID    NUMBER(12,0),
    CUSTOMER_SINCE    DATE,
    CUSTOMER_CLASS    VARCHAR2(40 BYTE),
    SUGGESTIONS       VARCHAR2(40 BYTE),
    DOB               DATE,
    MAILSHOT          VARCHAR2(1 BYTE),
    PARTNER_MAILSHOT  VARCHAR2(1 BYTE),
    PREFERRED_ADDRESS NUMBER(12,0),
    PREFERRED_CARD    NUMBER(12,0),
    CONSTRAINT CUSTOMERS_PK PRIMARY KEY (CUSTOMER_ID)
  ) tablespace set soe_ts_set
  partition by consistent hash (CUSTOMER_ID) partitions auto
  STORAGE (INITIAL 4M NEXT 4M);

CREATE SHARDED TABLE CARD_DETAILS
  (
    CARD_ID       NUMBER(12,0) NOT NULL,
    CUSTOMER_ID   VARCHAR2(40 BYTE) NOT NULL,
    CARD_TYPE     VARCHAR2(30 BYTE),
    CARD_NUMBER   NUMBER(12,0),
    EXPIRY_DATE   DATE,
    IS_VALID      VARCHAR2(1 BYTE),
    SECURITY_CODE NUMBER(6,0),
    CONSTRAINT CARD_DETAILS_PK PRIMARY KEY (CUSTOMER_ID,CARD_ID),
    CONSTRAINT CARD_DETAILS_CUSTOMER_ID_FK FOREIGN KEY(CUSTOMER_ID) REFERENCES CUSTOMERS ON DELETE CASCADE
  ) tablespace set soe_ts_set
  partition by reference (CARD_DETAILS_CUSTOMER_ID_FK)
  STORAGE (INITIAL 4M NEXT 4M);

CREATE SHARDED TABLE ADDRESSES
  (
    ADDRESS_ID       NUMBER(12,0) NOT NULL,
    CUSTOMER_ID      VARCHAR2(40 BYTE) NOT NULL,
    DATE_CREATED     DATE,
    HOUSE_NO_OR_NAME VARCHAR2(60 BYTE),
    STREET_NAME      VARCHAR2(60 BYTE),
    TOWN             VARCHAR2(60 BYTE),
    COUNTY           VARCHAR2(60 BYTE),
    COUNTRY          VARCHAR2(60 BYTE),
    POST_CODE        VARCHAR2(12 BYTE),
    ZIP_CODE         VARCHAR2(12 BYTE),
    CONSTRAINT ADDRESSES_PK PRIMARY KEY (CUSTOMER_ID,ADDRESS_ID),
    CONSTRAINT ADDRESSES_CUSTOMER_ID_FK FOREIGN KEY(CUSTOMER_ID) REFERENCES CUSTOMERS ON DELETE CASCADE
  ) tablespace set soe_ts_set
  partition by reference (ADDRESSES_CUSTOMER_ID_FK)
  STORAGE (INITIAL 4M NEXT 4M);

CREATE SHARDED TABLE ORDERS
  (
    ORDER_ID                NUMBER(12,0) NOT NULL,
    ORDER_DATE              TIMESTAMP (6) WITH LOCAL TIME ZONE,
    ORDER_MODE              VARCHAR2(8 BYTE),
    CUSTOMER_ID             VARCHAR2(40 BYTE) NOT NULL,
    ORDER_STATUS            NUMBER(2,0),
    ORDER_TOTAL             NUMBER(8,2),
    SALES_REP_ID            NUMBER(6,0),
    PROMOTION_ID            NUMBER(6,0),
    WAREHOUSE_ID            NUMBER(6,0),
    DELIVERY_TYPE           VARCHAR2(15 BYTE),
    COST_OF_DELIVERY        NUMBER(6,0),
    WAIT_TILL_ALL_AVAILABLE VARCHAR2(15 BYTE),
    DELIVERY_ADDRESS_ID     NUMBER(12,0),
    CUSTOMER_CLASS          VARCHAR2(30 BYTE),
    CARD_ID                 NUMBER(12,0),
    INVOICE_ADDRESS_ID      NUMBER(12,0),
    CONSTRAINT PK_ORDERS PRIMARY KEY (CUSTOMER_ID, ORDER_ID),
    CONSTRAINT ORDERS_CUSTOMER_ID_FK FOREIGN KEY(CUSTOMER_ID) REFERENCES CUSTOMERS ON DELETE CASCADE
  ) tablespace set soe_ts_set
  partition by reference (ORDERS_CUSTOMER_ID_FK)
  STORAGE (INITIAL 4M NEXT 4M);

CREATE SHARDED TABLE ORDER_ITEMS
  (
    ORDER_ID           NUMBER(12,0) NOT NULL,
    LINE_ITEM_ID       NUMBER(3,0) NOT NULL,
    PRODUCT_ID         NUMBER(6,0),
    CUSTOMER_ID        VARCHAR2(40 BYTE) NOT NULL,
    UNIT_PRICE         NUMBER(8,2),
    QUANTITY           NUMBER(8,0),
    DISPATCH_DATE      DATE,
    RETURN_DATE        DATE,
    GIFT_WRAP          VARCHAR2(20 BYTE),
    CONDITION          VARCHAR2(20 BYTE),
    SUPPLIER_ID        NUMBER(6,0),
    ESTIMATED_DELIVERY DATE,
    CONSTRAINT  ORDER_ITEMS_PK primary key (CUSTOMER_ID, ORDER_ID, PRODUCT_ID),
    CONSTRAINT  ORDER_ITEMS_ORDER_ID_FK foreign key (CUSTOMER_ID, ORDER_ID)
    references Orders on delete cascade
  ) tablespace set soe_ts_set
  partition by reference (ORDER_ITEMS_ORDER_ID_FK)
  STORAGE (INITIAL 4M NEXT 4M);

CREATE SHARDED TABLE LOGON
  (
    LOGON_ID            NUMBER(12,0) NOT NULL,
    CUSTOMER_ID         VARCHAR2(40 BYTE) NOT NULL,
    LOGON_DATE          DATE,
    CONSTRAINT LOGON_CUSTOMER_ID_FK FOREIGN KEY(CUSTOMER_ID) REFERENCES CUSTOMERS ON DELETE CASCADE
  ) tablespace set soe_ts_set
  partition by reference (LOGON_CUSTOMER_ID_FK)
  STORAGE (INITIAL 4M NEXT 4M);

CREATE DUPLICATED TABLE ORDERENTRY_METADATA
  (
    METADATA_KEY        VARCHAR2(30 BYTE) NOT NULL,
    METADATA_VALUE      VARCHAR2(30 BYTE) NOT NULL
  ) tablespace products
  STORAGE (INITIAL 1M NEXT 1M);

CREATE DUPLICATED TABLE PRODUCT_DESCRIPTIONS
  (
    PRODUCT_ID          NUMBER(6,0) NOT NULL,
    LANGUAGE_ID         VARCHAR2(3 BYTE),
    TRANSLATED_NAME     NVARCHAR2(50),
    TRANSLATED_DESCRIPTION NVARCHAR2(2000),
    CONSTRAINT  PRODUCT_DESCRIPTIONS_PK primary key (PRODUCT_ID)
  ) tablespace products
  STORAGE (INITIAL 1M NEXT 1M);

CREATE DUPLICATED TABLE PRODUCT_INFORMATION
  (
    PRODUCT_ID          NUMBER(6,0) NOT NULL,
    PRODUCT_NAME        VARCHAR2(50 BYTE),
    PRODUCT_DESCRIPTION VARCHAR2(2000 BYTE),
    CATEGORY_ID         NUMBER(4,0),
    WEIGHT_CLASS        NUMBER(1,0),
    WARRANTY_PERIOD     INTERVAL YEAR (2) TO MONTH,
    SUPPLIER_ID         NUMBER(6,0),
    PRODUCT_STATUS      VARCHAR2(20 BYTE),
    LIST_PRICE          NUMBER(8,2),
    MIN_PRICE           NUMBER(8,2),
    CATALOG_URL         VARCHAR2(50 BYTE),
    CONSTRAINT  PRODUCT_INFORMATION_PK primary key (PRODUCT_ID)
  ) tablespace products
  STORAGE (INITIAL 1M NEXT 1M);